
// const ApiService = {

//     // baseURL : "http://13.232.192.186/",

//     getMethod: (url, body) => {
//         return fetch(`http://13.232.192.186/${url}`).then(res => res.json());
//     }

// }

const baseUrl = 'http://3.6.73.209/'

export const get = (url) => {
    return fetch(`${baseUrl}${url}`).then(res => res.json());
}

export const post = (url, body) => {
    return fetch(`${baseUrl}${url}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(body)
    }).then(async res => {
        if (res.ok) {
            return res.json();
        } else {
            throw await res.json();
        }
    });
}