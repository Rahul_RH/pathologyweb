

import { post, get } from './api.service'
import { getUserDetails } from './localStorage.service'



    const currentUserData = getUserDetails()
    let rangeTypeList = []
    let unitsList = []
    export const callRangeTypeList = () => {
         get('TestItems/getAllRangeTypes').then(res => {
            if(res.code === 200) {
                setRangeTypeList(res.Response)
            }
        }).catch(err => {

        })
    }
    const setRangeTypeList = (data) => {
        rangeTypeList = data
    }
    export const getRangeTypeList = () => {
        return rangeTypeList
    }
    export const callUnitsList = () => {
        return get('TestItems/getAllUnitTypes').then(res => {
            if(res.code === 200) {
                setUnitsList(res.Response)
            }
        }).catch(err => {

        })
    }
     const setUnitsList = (data) => {
        unitsList = data
    }
    export const getUnitsList = () => {
        return unitsList
    }
