

import { post } from './api.service'
import { setUserDetails } from './localStorage.service'


class UserService {
    // apiService = ApiService.getMethod();


    userLogin(body) {
        return post('Users/loginUsers', body).then(res => {
            setUserDetails(res.Response)
            return res
        })
    }
}

export default UserService