
import { post } from './api.service'
import { getUserDetails } from './localStorage.service'


class TestReportService {
    currentUserData = getUserDetails()
    getReportParameters(body) {
        return post('CaseRegistration/getReportFields', body)
    }

    createReport(body) {
        return post('CaseRegistration/createTestReports', body)
    }

}

export default TestReportService