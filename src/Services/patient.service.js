
import { post, get } from './api.service'

class PatientService {

    createPatient(body) {
        return post('createPatient', body)
    }

    getGenderList() {
        return get('getAllGender')
    }

    getReligionList() {
        return get('getAllReligions')
    }

    getLanguageList() {
        return get('getAllLanguages')
    }

    getRelationshipList() {
        return get('getAllRelationship')
    }

   
}

export default PatientService