

export const setUserDetails = (userData) => {
    localStorage.setItem('userdetails', JSON.stringify(userData))
}

export const getUserDetails = () => {
    return JSON.parse(localStorage.getItem('userdetails'))
}

export const destroyUserDetails = () => {
    localStorage.removeItem('userdetails')
}

export const createTestReport = (reportData) => {
    localStorage.setItem('test_report', JSON.stringify(reportData))
}

export const getTestReport = () => {
    return JSON.parse(localStorage.getItem('test_report'))
}

export const destroyReport = () => {
    localStorage.removeItem('test_report')
}
