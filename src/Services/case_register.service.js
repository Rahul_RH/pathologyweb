

import { post } from './api.service'

class CaseRegistraionService {

    // createPatient(body) {
    //     return post('createPatient', body)
    // }

    getTestItems(body) {
        return post('TestItems/getAll', body)
    }

    createTest(body) {
        return post('CaseRegistration/create', body)
    }

    getAllCaseRegistration(body) {
        return post('CaseRegistration/getAll', body)
    }

    // ********* Start:  API Services For get orders based on Status ************

    getAllPendingTests(body) {
        return post('CaseRegistration/getCasesBySamplePendingStatus', body)
    }

    getAllSampleCollectedTests(body) {
        return post('CaseRegistration/getCasesBySampleCollectedStatus', body)
    }

    getCasesBySampleForwardStatus(body) {
        return post('CaseRegistration/getCasesBySampleForwardStatus', body)
    }

    getCasesByLabReceivedStatus(body) {
        return post('CaseRegistration/getCasesByLabReceivedStatus', body)
    }

    getCasesBySampleRejectedStatus(body) {
        return post('CaseRegistration/getCasesBySampleRejectedStatus', body)
    }

    getCasesByTestIncompleteStatus(body) {
        return post('CaseRegistration/getCasesByTestIncompleteStatus', body)
    }

    getCasesByTestCompleteStatus(body) {
        return post('CaseRegistration/getCasesByTestCompleteStatus', body)
    }

    getCasesByTestRejectStatus(body) {
        return post('CaseRegistration/getCasesByTestRejectStatus', body)
    }

    getCasesByTestApprovedStatus(body) {
        return post('CaseRegistration/getCasesByTestApprovedStatus', body)
    }

    getCasesByTestHoldStatus(body) {
        return post('CaseRegistration/getCasesByTestHoldStatus', body)
    }
    getCasesByTestCancelledStatus(body) {
        return post('CaseRegistration/getCasesByTestCancelledStatus', body) 
    }

    getCasesByCompletedStatus(body) {
        return post('CaseRegistration/getCasesByCompletedStatus', body)
    }

// ********* End :  API Services For get orders based on Status ************

    getReferralList(body) {
        return post('referrals/getAllReferrals', body)
    }

    getCaseById(body) {
        return post('CaseRegistration/getById', body)
    }

    getTestReportItems(body) {
        return post('TestItems/getById', body)
    }

    // ************ Start : API Services To change orders Status ***********************
    samplePendingStatus(body) {
        return post('status/samplePendingStatus', body)
    }
    sampleCollectedStatus(body) {
        return post('status/sampleCollectedStatus', body)
    }
    testCancelledStatus(body) {
        return post('status/testCancelledStatus', body)
    }
    sampleForwardStatus(body) {
        return post('status/sampleForwardStatus', body)
    }
    labReceivedStatus(body) {
        return post('status/labReceivedStatus', body)
    }
    sampleRejectedStatus(body) {
        return post('status/sampleRejectedStatus', body)
    }
    testIncompleteStatus(body) {
        return post('status/testIncompleteStatus', body)
    }
    testCompleteStatus(body) {
        return post('status/testCompleteStatus', body)
    }
    testRejectStatus(body) {
        return post('status/testRejectStatus', body)
    }
    testApprovedStatus(body) {
        return post('status/testApprovedStatus', body)
    }
    testHoldStatus(body) {
        return post('status/testHoldStatus', body)
    }
    completedStatus(body) {
        return post('status/completedStatus', body)
    }


}

export default CaseRegistraionService