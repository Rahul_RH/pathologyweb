import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom'
import './App.css';
import { Login } from './Components/Login/login';
import Header from './Components/Header/header';
import UserRegister from './Components/user_register/user_register';
import Dashboard from './Components/Dashboard/dashboard';
import { getUserDetails } from './Services/localStorage.service';
import {callRangeTypeList, callUnitsList} from './Services/app_data.service';

// import CaseRegistration from './Components/Case Registration/case_registration';
// import { PrintReport } from './Components/Print Report/print_report';
// import { BillPage } from './Components/Bill Page/bill_page';





// import CaseRegistration from './Components/Case Registration/case_registration';
// import { BillPage } from './Components/Bill Page/bill_page';
// import { CreateTestReport } from './Components/Create Test Report/create_test_report';







function App() {
  callRangeTypeList()
  callUnitsList()
  
  
  // const history = useHistory();

  // history.push("/dashboard");
  // console.log(getUserDetails())
  // if (!getUserDetails()) {
  //   return (
  //     <div>
  //       <Dashboard />
  //     </div>
  //   )

  // } else {
  //   return (
  //     <div>
  //       <Login />
  //     </div>
  //   )

  // }

  // const userDetails = {
  //   path: history.location.pathname
  // };
  return (
    <div className="App">
      <Header></Header>
      {getUserDetails() && < Redirect from="/" to="/dashboard" />}
      <Switch>

        <Route exact path='/' component={Login} />
        <Route exact path='/register' component={UserRegister} />
        {<Route path='/dashboard' component={Dashboard} />}

      </Switch>
      <div style={{height : 50+'px'}}></div>
    </div>
  );
}

export default App;
