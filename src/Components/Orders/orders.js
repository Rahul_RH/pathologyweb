import React, { Component } from 'react';
import { Tabs, Tab } from 'react-bootstrap';
import './orders.css'
import AcceptOrder from '../Accept Order/accept_order';


class Orders extends Component {

    constructor(props) {
        super(props);
        this.state = {
            key: 'getSamplePending'
        };
    }
    render() {
        return (
            <div className="order-body">
                <h5>Orders</h5>
                <Tabs id="controlled-tab-example"
                    activeKey={this.state.key}
                    onSelect={(k) => this.setState({ key: k })}>
                    <Tab eventKey="getSamplePending" title="Sample Pending ">
                        {this.state.key === 'getSamplePending' && <AcceptOrder {...this.props} status={'getSamplePending'}/>}
                    </Tab>
                    <Tab eventKey="getSampleReceived" title="Sample Collected">
                        {this.state.key === 'getSampleReceived' && <AcceptOrder {...this.props} status={'getSampleReceived'}/>}
                    </Tab>
                    {/* <Tab eventKey="sampleAccepted" title="Sample Accepted" >
                        {this.state.key === 'sampleAccepted' && <AcceptOrder {...this.props} status={'sampleAccepted'}/>}
                    </Tab> */}
                    <Tab eventKey="getSampleRejected" title="Sample Rejected">
                        {this.state.key === 'getSampleRejected' && <AcceptOrder {...this.props} status={'getSampleRejected'}/>}
                    </Tab>
                    <Tab eventKey="getSampleForward" title="Sample Forwarded">
                        {this.state.key === 'getSampleForward' && <AcceptOrder {...this.props} status={'getSampleForward'}/>}
                    </Tab>
                    <Tab eventKey="getLabReceived" title="Lab Received" >
                        {this.state.key === 'getLabReceived' && <AcceptOrder {...this.props} status={'getLabReceived'}/>}
                    </Tab>

                    {/* <Tab eventKey="getTestIncomplete" title="Test Incomplete" >
                        {this.state.key === 'getTestIncomplete' && <AcceptOrder {...this.props} status={'getTestIncomplete'}/>}
                    </Tab> */}
                    <Tab eventKey="getTestReject" title="Test Reject" >
                        {this.state.key === 'getTestReject' && <AcceptOrder {...this.props} status={'getTestReject'}/>}
                    </Tab>
                    <Tab eventKey="getTestComplete" title="Test Complete" >
                        {this.state.key === 'getTestComplete' && <AcceptOrder {...this.props} status={'getTestComplete'}/>}
                    </Tab>
                    <Tab eventKey="getTestApproved" title="Test Approved" >
                        {this.state.key === 'getTestApproved' && <AcceptOrder {...this.props} status={'getTestApproved'}/>}
                    </Tab>
                    <Tab eventKey="getTestHold" title="Test Hold" >
                        {this.state.key === 'getTestHold' && <AcceptOrder {...this.props} status={'getTestHold'}/>}
                    </Tab>
                    <Tab eventKey="getTestCanceled" title="Test Canceled" >
                        {this.state.key === 'getTestCanceled' && <AcceptOrder {...this.props} status={'getTestCanceled'}/>}
                    </Tab>
                    

                    {/* <Tab eventKey="doctorPending" title="Doc Review Pending" >
                        {this.state.key === 'doctorPending' && <AcceptOrder status={'doctorPending'}/>}
                    </Tab> */}
                    <Tab eventKey="getCompleted" title="Completed" >
                        {this.state.key === 'getCompleted' && <AcceptOrder {...this.props} status={'getCompleted'}/>}
                    </Tab>
                </Tabs>
            </div>

        );
    }
}

export default Orders;