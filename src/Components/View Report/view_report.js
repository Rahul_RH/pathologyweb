import React, { Component } from "react";
import CaseRegistraionService from "../../Services/case_register.service";
import {Button, Container, Form, Col, Row, Card, Table} from "react-bootstrap";
import "./view_report.css";
import { getUserDetails } from "../../Services/localStorage.service";
import TestReportService from "../../Services/test_report.service";
import { getRangeTypeList, getUnitsList } from "../../Services/app_data.service";


export class ViewReport extends Component {
  _isMounted = false;
  caseRegistraionService = new CaseRegistraionService();
  testReportService = new TestReportService()
  rangeTypeList = getRangeTypeList() || []
  unitTypeList = getUnitsList() || []
  reportDetails;
  constructor(props) {
    super(props);
    this.state = {
      testDetails: {},
      reportItems: [],
    };
  }

  componentDidMount() {
    this._isMounted = true;
    let obj = {
      case_test_map_id: this.props.location.state.response.id,
    };
    console.log(this.props.location.state.response)
    this.testReportService.getReportParameters(obj).then(res => {
      if (this._isMounted) {

        let temp = res.Response.childTestItems.sort((a, b) => a.test_item_id.display_order - b.test_item_id.display_order)
        console.log(res.Response)
        if (res.Response.childTestItems.length > 0) {
          this.setState({ testDetails: res.Response, reportItems: temp });
        } else {
          this.setState({ testDetails: res.Response, reportItems: [res.Response] })
        }
      }

    }).catch(err => {
      alert(err.message)
    })
  }

  submitReport = (event) => {
    event.preventDefault();
    let _reportItems = this.state.reportItems.map(data => {
      data.user_id = getUserDetails().id
      return data
    })
    let obj = {
      "case_test_map_ids": _reportItems,
      "pathology_id": getUserDetails().pathology_id.id,
      "case_id": this.state.testDetails.case_id.id
    }
    this.testReportService.createReport(obj).then(res => {
      if (res.code === 200) {
        console.log(res)
        alert(res.message);
        this.props.history.goBack();
      }

    }).catch(err => {
      alert(err.message)
    })

  };

  cancelReport = () => {
    this.props.history.goBack();
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  setValue = (value, index) => {
    this.state.reportItems.map((data, i) => {
      if (i === index) {
        data.actual_value = value;
      }
      return data;
    });
    this.setState({ reportItems: this.state.reportItems });
  };

  setRemark = (value, index) => {
    this.state.reportItems.map((data, i) => {
      if (i === index) {
        data.remarks = value;
      }
      return data;
    });
    this.setState({ reportItems: this.state.reportItems });
  };

  setRangeType = (value, index) => {
    this.state.reportItems.map((data, i) => {
      if (i === index) {
        data.range_type_id = parseInt(value);
      }
      return data;
    });
    this.setState({ reportItems: this.state.reportItems });
  }

  setUnitId = (value, index) => {
    this.state.reportItems.map((data, i) => {
      if (i === index) {
        data.unit_id = parseInt(value);
      }
      return data;
    });
    this.setState({ reportItems: this.state.reportItems });
  }

  render() {
    // console.log(this.state.reportItems)
    return (
      <div>
        <Container>
          <Row className="justify-content-center">
            <Card className="text-center patient-card">
              <Card.Header>Test Report</Card.Header>
              <Card.Body className="text-left">
                <Row className="header-block">
                  <Col>
                    <div className="header-names">Patient</div>
                    <Row>
                      <Col>
                        <div>
                          ID:{" "}
                          {
                            this.props.location.state.response.case_id
                              .patient_id.patient_id
                          }
                        </div>
                        <div>
                          Name/Gender:
                          {this.props.location.state.response.case_id.patient_id
                            .first_name + " "}
                          {
                            this.props.location.state.response.case_id
                              .patient_id.first_name
                          }
                          /
                          {this.props.location.state.response.case_id.patient_id
                            .gender.gender_name || ""}
                        </div>
                      </Col>
                      <Col>
                        <div>
                          Age:{" "}
                          {
                            this.props.location.state.response.case_id
                              .patient_id.age
                          }
                        </div>
                        <div>
                          Mobile:{" "}
                          {
                            this.props.location.state.response.case_id
                              .patient_id.patient_contact_no
                          }
                        </div>
                      </Col>
                    </Row>
                  </Col>
                  <Col>
                    <div className="header-names">Referred by</div>
                    <Row>
                      <Col>
                        <div>
                          Name:{" "}
                          {
                            this.props.location.state.response.case_id
                              .referral_id.doctor_name
                          }
                        </div>
                        <div>Mobile: {""}</div>
                      </Col>
                      <Col></Col>
                    </Row>
                  </Col>
                </Row>
                <Form onSubmit={this.submitReport}>
                  <Table striped bordered>
                    <thead>
                      <tr>
                        <th></th>
                        <th></th>
                        <th colSpan={3}>Range</th>
                        <th></th>
                        {/* <th></th> */}
                      </tr>
                      <tr>
                        <th>Name</th>
                        <th>Value</th>
                        <th>Min</th>
                        <th>type</th>
                        <th>Max</th>
                        <th>Unit</th>
                        {/* <th>Remark</th> */}
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.reportItems.length > 0 && this.state.reportItems.map((data, index) => {
                        if (data.range_type_id && data.range_type_id <= 4) {

                          return (

                            <tr key={index}>
                              <td>{data.test_item_id.test_name}</td>
                              <td><Form.Control type="text"
                                defaultValue={data.reading || ''}
                                onChange={(e) => data.reading = e.target.value} />
                              </td>
                              <td><Form.Control type="text"
                                defaultValue={data.min_range || ''}
                                onChange={(e) => data.min_range = e.target.value} />
                              </td>
                              <td><Form.Control as="select"
                                value={data.range_type_id || ''}
                                onChange={(e) => this.setRangeType(e.target.value, index)}>
                                {/* <option value=''>Select...</option> */}
                                {this.rangeTypeList.map((data, index) => {
                                  return (
                                    <option key={index} value={data.id}>{data.type_name}</option>
                                  )
                                })}
                              </Form.Control>
                              </td>
                              <td><Form.Control type="text"
                                defaultValue={data.max_range || ''}
                                onChange={(e) => data.max_range = e.target.value} /></td>
                              <td>
                                <Form.Control as="select"
                                  value={data.unit_id || ''}
                                  onChange={(e) => this.setUnitId(e.target.value, index)}>
                                  {/* <option value=''>Select...</option> */}
                                  {this.unitTypeList.map((data, index) => {
                                    return (
                                      <option key={index} value={data.id}>{data.unit_name}</option>
                                    )
                                  })}
                                </Form.Control></td>
                              {/* <td><Form.Control  type="text"
                                                defaultValue={data.comment || ''} 
                                                onChange={(e) => this.setRemark(e.target.value, index)}/></td> */}
                            </tr>
                          )
                        } else if (data.range_type_id && data.range_type_id === 7) {
                          return (
                            <tr key={index}>
                              <td>{data.test_item_id.test_name} </td>
                              <td colSpan="4"><Form.Control type="text"
                                defaultValue={data.reading || ''}
                                onChange={(e) => this.setValue(e.target.value, index)} />
                              </td>
                              <td><Form.Control as="select"
                                value={data.range_type_id || ''}
                                onChange={(e) => this.setRangeType(e.target.value, index)}>
                                {/* <option value=''>Select...</option> */}
                                {this.rangeTypeList.map((data, index) => {
                                  return (
                                    <option key={index} value={data.id}>{data.type_name}</option>
                                  )
                                })}
                              </Form.Control>
                              </td>
                            </tr>
                          )
                        } else if (data.range_type_id && data.range_type_id === 5) {
                          return (
                            <tr key={index}>
                              <td>{data.test_item_id.test_name} </td>
                              <td colSpan="4"><textarea className="form-control" rows="3"
                                defaultValue={data.reading || ''}
                                onChange={(e) => this.setValue(e.target.value, index)}></textarea>
                              </td>
                              <td><Form.Control as="select"
                                value={data.range_type_id || ''}
                                onChange={(e) => this.setRangeType(e.target.value, index)}>
                                {/* <option value=''>Select...</option> */}
                                {this.rangeTypeList.map((data, index) => {
                                  return (
                                    <option key={index} value={data.id}>{data.type_name}</option>
                                  )
                                })}
                              </Form.Control>
                              </td>
                            </tr>
                          )
                        } else {
                          return (
                            <tr key={index}>
                              <td>{data.test_item_id.test_name} </td>
                            </tr>
                          )
                        }
                      })}
                    </tbody>
                  </Table>
                  <Button variant="primary" type="submit">Submit</Button>
                  <Button variant="primary" type="button" onClick={this.cancelReport}>Cancel</Button>
                </Form>
              </Card.Body>
            </Card>
          </Row>
        </Container>
      </div>
    )
  }
}
