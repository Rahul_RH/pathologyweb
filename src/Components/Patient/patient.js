import React, { Component } from "react";
import { Button, Container, Form, Col, Row, Card } from "react-bootstrap";
import "./patient.css";
import PatientService from "../../Services/patient.service";
import { getUserDetails } from "../../Services/localStorage.service";
import ReactToPrint from "react-to-print";
import {getRangeTypeList} from "../../Services/app_data.service";


export class Patient extends Component {
    _isMounted = false;
    patientService = new PatientService()
    indianStates = [
    "Andaman and Nicobar Islands",
    "Andhra Pradesh",
    "Arunachal Pradesh",
    "Assam",
    "Bihar",
    "Chandigarh",
    "Chhattisgarh",
    "Dadra and Nagar Haveli",
    "Daman and Diu",
    "Delhi",
    "Goa",
    "Gujarat",
    "Haryana",
    "Himachal Pradesh",
    "Jammu and Kashmir",
    "Jharkhand",
    "Karnataka",
    "Kerala",
    "Ladakh",
    "Lakshadweep",
    "Madhya Pradesh",
    "Maharashtra",
    "Manipur",
    "Meghalaya",
    "Mizoram",
    "Nagaland",
    "Odisha",
    "Puducherry",
    "Punjab",
    "Rajasthan",
    "Sikkim",
    "Tamil Nadu",
    "Telangana",
    "Tripura",
    "Uttar Pradesh",
    "Uttarakhand",
    "West Bengal"]
    constructor(props) {
        super(props)
        this.state = {

            first_name: "",
            last_name: "",
            email_id: "",
            patient_contact_no: 0,
            ref_contact_no: 0,
            gender: 0,
            relationship_id: 0,
            marital_status: 2,
            dob: "",
            age: 0,
            ageMonths: 0,
            ageDays: 0,
            religion_id: 0,
            language_id: 0,
            street: "1234 Street",
            area: "",
            city: "",
            state: "",
            zip: 0,
            ref_doc: "",
            genderList : [],
            religionList : [],
            languageList : [],
            relationshipList : []
        }
        console.log(getRangeTypeList())
      }

    getGenderList = () => {
        this.patientService.getGenderList().then(res => {
           
            if (this._isMounted && res.code === 200 && res.Response.length > 0) {
                this.setState({genderList : res.Response})
            }
            
        }).catch(err => {
            alert(err.message)
        })
    }

    getReligionList = () => {
        this.patientService.getReligionList().then(res => {
          
            if (this._isMounted && res.code === 200 && res.Response.length > 0) {
                this.setState({religionList : res.Response, religion_id : res.Response[0].id})
            }
           
        }).catch(err => {
            alert(err.message)
        })
    }

    getLanguageList = () => {
        // console.log("test")
        this.patientService.getLanguageList().then(res => {
            
            if (this._isMounted && res.code === 200 && res.Response.length > 0) {
                this.setState({languageList : res.Response, language_id : res.Response[0].id})
                
            }
            
        }).catch(err => {
            alert(err.message)
        })
    }

    getRelationshipList = () => {
        this.patientService.getRelationshipList().then(res => {
         
            if (this._isMounted && res.code === 200 && res.Response.length > 0) {
                this.setState({relationshipList : res.Response, relationship_id : res.Response[0].id})
            }
            
        }).catch(err => {
            alert(err.message)
        })
    }

    componentDidMount() {
        this._isMounted = true;
        this.getGenderList()
        this.getReligionList()
        this.getLanguageList()
        this.getRelationshipList()
    }
    componentWillUnmount() {
        this._isMounted = false;
      }

    createPatient = (event) => {
        event.preventDefault();
        // console.log(this.state);
        let obj = {
            pathology_id: getUserDetails().pathology_id.id,
            first_name: this.state.first_name,
            last_name: this.state.last_name,
            email_id: this.state.email_id,
            patient_contact_no: this.state.patient_contact_no,
            ref_contact_no: this.state.ref_contact_no,
            gender: parseInt(this.state.gender),
            relationship_id : this.state.relationship_id,
            marital_status_id: this.state.marital_status,
            dob: this.state.dob,
            age: this.state.age,
            months : this.state.ageMonths,
            days : this.state.ageDays,
            religion_id: this.state.religion_id,
            language_id: this.state.language_id,
            street: this.state.street,
            area: this.state.area,
            city: this.state.city,
            state: this.state.state,
            zip: parseInt(this.state.zip)
            // ref_doc: '155c8ece-6b41-4b78-9459-33041fb2cd425'
        }
        this.patientService.createPatient(obj).then(res => {
            // console.log(res)
            this.props.callbackFromPatient(res.Response)
        }).catch(err => {
            alert(err.message)
        })
    }

 
  setContactNumber = (event) => {
    this.setState({ patient_contact_no: event.target.value });
  };
    render() {
        
        return (
            <div>
               
                <Container >
                    <Row className="justify-content-center">
                        <Card className="text-center patient-card">
                            <Card.Header>Create Patient Details</Card.Header>
                            <Card.Body className="text-left">
                                <Form onSubmit={this.createPatient}>
                                    <Form.Row>
                                        <Form.Group as={Col} >
                                            <Form.Label>Patient Contact Number *</Form.Label>
                                            <Form.Control type="tel" pattern="[23456789][0-9]{9}" required
                                                placeholder="Enter Patient Contact Number"
                                                value={this.state.patient_contact_no || ''}
                                                onChange={(e) => this.setContactNumber(e)} />
                                        </Form.Group>

                                    </Form.Row>
                                    <Form.Row>
                                        <Form.Group as={Col} >
                                            <Form.Label>First Name *</Form.Label>
                                            <Form.Control type="text" required
                                                placeholder="Enter First Name"
                                                value={this.state.first_name || ''}
                                                onChange={(e) => this.setState({ first_name: e.target.value })} />
                                        </Form.Group>

                                        <Form.Group as={Col} >
                                            <Form.Label>Last Name *</Form.Label>
                                            <Form.Control type="text" required
                                                placeholder="Enter First Name"
                                                value={this.state.last_name || ''}
                                                onChange={(e) => this.setState({ last_name: e.target.value })} />
                                        </Form.Group>
                                    </Form.Row>
                                    <Form.Row>
                                    <Form.Group as={Col} >
                                            <Form.Label>Relationship *</Form.Label>
                                            <Form.Control as="select" required
                                                value={this.state.relationship_id || ''}
                                                onChange={(e) => e.target.value && this.setState({ relationship_id: e.target.value })}>
                                                <option value=''>Select...</option>
                                                {this.state.relationshipList.map((data, index) => {
                                                    return (
                                                        <option key={index} value={data.id}>{data.relationship_name}</option>         
                                                    )
                                                })}
                                            </Form.Control>
                                        </Form.Group>
                                        <Form.Group as={Col} >
                                            <Form.Label>Gender *</Form.Label>
                                            <Form.Control as="select" required
                                                value={this.state.gender || ''}
                                                onChange={(e) => e.target.value && this.setState({ gender: e.target.value })}>
                                                <option value=''>Select...</option>
                                                {this.state.genderList.map((data, index) => {
                                                    return (
                                                        <option key={index} value={data.id}>{data.gender_name}</option>         
                                                    )
                                                })}
                                            </Form.Control>
                                        </Form.Group>

                                      
                                    </Form.Row>
                                    <Form.Row>
                                        <Form.Group as={Col} >
                                            <Form.Label>Email</Form.Label>
                                            <Form.Control type="email"
                                                placeholder="Enter email"
                                                value={this.state.email_id || ''}
                                                onChange={(e) => this.setState({ email_id: e.target.value })} />
                                        </Form.Group>
                                        {/* <Form.Label style={{margin: 28+'px'+ 15+'px'}}>Age</Form.Label> */}
                                                <Form.Row>
                                                
                                                <Form.Group as={Col} >
                                            <Form.Label>Age : Year</Form.Label>
                                            <Form.Control type="number"  min="0" 
                                                placeholder="Year"
                                                value={this.state.age || ''}
                                                onChange={(e) => this.setState({ age: e.target.value })} />
                                        </Form.Group>
                                        <Form.Group as={Col} >
                                            <Form.Label>Months</Form.Label>
                                            <Form.Control type="number"  min="0" 
                                                placeholder="Month"
                                                value={this.state.ageMonths || ''}
                                                onChange={(e) => this.setState({ ageMonths: e.target.value })} />
                                        </Form.Group>
                                        <Form.Group as={Col} >
                                            <Form.Label>Days</Form.Label>
                                            <Form.Control type="number"  min="0" 
                                                placeholder="Days"
                                                value={this.state.ageDays || ''}
                                                onChange={(e) => this.setState({ ageDays: e.target.value })} />
                                        </Form.Group>
                                                </Form.Row>
                                          
                                    </Form.Row>
                                   

                                    <Form.Row>
                                        <Form.Group as={Col} >
                                            <Form.Label>Maritual Status *</Form.Label>
                                            <Form.Control as="select" required
                                                value={this.state.marital_status || ''}
                                                onChange={(e) => this.setState({ marital_status: e.target.value })}>
                                                <option value="">Select...</option>
                                                <option value='1'>Married</option>
                                                <option value='2'>Unmarried</option>
                                                </Form.Control>
                    </Form.Group>

                    
                  {/* </Form.Row> */}

                                        <Form.Group as={Col} >
                                            <Form.Label>Religion *</Form.Label>
                                            <Form.Control as="select" required
                                                value={this.state.religion_id || ''}
                                                onChange={(e) => e.target.value && this.setState({ religion_id: e.target.value })}>
                                                <option value=''>Select...</option>
                                                {this.state.religionList.map((data, index) => {
                                                    return (
                                                        <option key={index} value={data.id}>{data.religion_name}</option>         
                                                    )
                                                })}
                                            </Form.Control>
                                        </Form.Group>
                                        <Form.Group as={Col} >
                                            <Form.Label>Language *</Form.Label>
                                            <Form.Control as="select" required
                                                value={this.state.language_id || ''}
                                                onChange={(e) => e.target.value && this.setState({ language_id: e.target.value })}>
                                                <option value=''>Select...</option>
                                                {this.state.languageList.map((data, index) => {
                                                    return (
                                                        <option key={index} value={data.id}>{data.language_name}</option>         
                                                    )
                                                })}
                                                       </Form.Control>
                    </Form.Group>
                                                 </Form.Row>

                  
                  {/* <Form.Group>
                                        <Form.File id="exampleFormControlFile1" label="Upload patient docs" />
                                    </Form.Group> */}

                                    <Form.Group >
                                        <Form.Label>Street *</Form.Label>
                                        <Form.Control type="text" required
                                            placeholder="1234 Main St"
                                            value={this.state.street || ''}
                                            onChange={(e) => this.setState({ street: e.target.value })} />
                                    </Form.Group>

                  <Form.Group controlId="formGridAddress2">
                    <Form.Label>Area</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Area"
                      value={this.state.area || ""}
                      onChange={(e) => this.setState({ area: e.target.value })}
                    />
                  </Form.Group>

                                    <Form.Row>
                                        <Form.Group as={Col} controlId="formGridCity">
                                            <Form.Label>City *</Form.Label>
                                            <Form.Control type="text" required
                                                value={this.state.city || ''}
                                                onChange={(e) => this.setState({ city: e.target.value })} />
                                        </Form.Group>

                                        <Form.Group as={Col} controlId="formGridState">
                                            <Form.Label>State *</Form.Label>
                                            <Form.Control as="select" required
                                                value={this.state.state || ''}
                                                onChange={(e) => this.setState({ state: e.target.value })}>
                                                <option>Choose...</option>
                                                {this.indianStates.map((data, index) => {
                                                    return (
                                                    <option key={index} value={data}>{data}</option>
                                                    )
                                                })}
                                                
                                                {/* <option value='Karnataka'>Karnataka</option> */}
                                            </Form.Control>
                                        </Form.Group>

                                        <Form.Group as={Col} controlId="formGridZip">
                                            <Form.Label>Zip *</Form.Label>
                                            <Form.Control type="number" min="0" required value={this.state.zip || ''}
                                                onChange={(e) => this.setState({ zip: e.target.value })} />
                                        </Form.Group>
                                    </Form.Row>
                                    {/* <Form.Group>
                                        <Form.Check
                                            required
                                            label="Agree to terms and conditions"
                                            feedback="You must agree before submitting."
                                        />
                                    </Form.Group> */}

                                    <Button className="mx-2" variant="primary" type="submit">Submit</Button> 
                                    <Button className="mx-2" onClick={()=> this.props.history.goBack()}>Cancel</Button>
                                </Form>
                            </Card.Body>
                        </Card>
                    </Row>
                </Container>
                
            </div>
        )
    }
}

export default Patient;
