import React, { useState } from "react";
import { Button, Modal, Table, Form } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import "./Users.css";

const Users = () => {
  const [showPassword, setShowPassword] = useState(false);
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const handleClosePassword = () => setShowPassword(false);
  const handleShowPassword = () => setShowPassword(true);
  const { replace } = useHistory();

  const employee = (val) => {
    replace(`/dashboard/users/employeeform?edit=${val}`);
  };

  const checkPassword = () => {
    if (password !== confirmPassword) {
      alert("Passwords don't match");
    }
  };

  return (
    <div className="order-body container p-3">
      <h5>Employee Details</h5>
      <Table responsive hover size="sm">
        <thead>
          <tr>
            <th>sl.No</th>
            <th>Name</th>
            <th>UserName</th>
            <th>Role</th>
            <th></th>
            <th></th>
            <th className=" mx-2">
              <Button
                className="px-2"
                variant="outline-success"
                size="sm"
                type="submit"
                onClick={() => employee("save")}
              >
                Add New
              </Button>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
            <td>
              <Button
                className="mx-2 bg-transparent border-0 text-primary"
                size="sm"
                onClick={handleShowPassword}
              >
                Change Password
              </Button>
            </td>
            <td>
              <Button
                className="mx-2 bg-transparent border-0 text-danger"
                size="sm"
              >
                Disable
              </Button>
            </td>
            <td>
              <Button
                className="mx-2 bg-transparent border-0 text-warning"
                size="sm"
                onClick={() => employee("update")}
              >
                Edit
              </Button>
            </td>
          </tr>
        </tbody>
      </Table>
      <Modal show={showPassword} onHide={handleClosePassword} animation={false}>
        <Modal.Header closeButton>
          <Modal.Title>Change Password</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div>
            <Form>
              <Form.Group controlId="formBasicPassword">
                <Form.Label>Enter New Password</Form.Label>
                <Form.Control
                  type="Password"
                  placeholder="Enter New Password"
                  onChange={(e) => setPassword(e.target.value)}
                />
                <Form.Text className="text-muted"></Form.Text>
              </Form.Group>
              <Form.Group controlId="formBasicPassword">
                <Form.Label>Confirm Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Confirm Password"
                  onChange={(e) => setConfirmPassword(e.target.value)}
                />
              </Form.Group>
              <Form.Group>
                <Button variant="danger" onClick={handleClosePassword}>
                  Close
                </Button>
                <Button
                  className="mx-2"
                  variant="success"
                  onClick={() => checkPassword()}
                >
                  Save Changes
                </Button>
              </Form.Group>
            </Form>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
};
export default Users;
