import React, { useState, Component } from "react";
import {
  Button,
  Modal,
  ModelBody,
  Table,
  Form,
  Row,
  Col,
  Card,
  Dropdown,
  DropdownButton,
  Container,
} from "react-bootstrap";
//import ModalBody from 'react-bootstrap/ModalBody';
import "./accounts.css";
import { FormGroup } from "@material-ui/core";

const Accounts = () => {
  return (
    <div className="order-body container p-3">
      <h5>Account Details</h5>
      <Container>
        <Card>
          <Card.Body>
            <Row>
              <Col sm={6}>
                <Form>
                  <Form.Group className="tdate">
                    <Form.Label>Date Range</Form.Label>
                    <Row>
                      <Col>
                        <Form.Control type="Date" placeholder="From" />
                      </Col>
                      <Col>
                        <Form.Control type="Date" placeholder="From" />
                      </Col>
                    </Row>
                  </Form.Group>
                  <Form.Group className="tdate">
                    <Button className="mx-2" variant="success" size="sm">
                      Yestraday
                    </Button>
                    <Button className="mx-2" variant="success" size="sm">
                      Today
                    </Button>
                    <Button className="mx-2" variant="success" size="sm">
                      Month Till Date
                    </Button>
                  </Form.Group>
                </Form>
              </Col>
              <Col sm={6}>
                <Form>
                  <Form.Group as={Row} className="dropdown">
                    <Form.Label>Filter Reference</Form.Label>
                    <Col sm="2">
                      <DropdownButton
                        id="dropdown-basic-button"
                        variant="success"
                        title="Dropdown button"
                        size="sm"
                      >
                        <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                        <Dropdown.Item href="#/action-2">
                          Another action
                        </Dropdown.Item>
                        <Dropdown.Item href="#/action-3">
                          Something else
                        </Dropdown.Item>
                      </DropdownButton>
                    </Col>
                  </Form.Group>
                  <Form.Group>
                    <Button className="mx-4" variant="success" size="sm">
                      Reset Filters
                    </Button>
                    <Button className="mx-2" variant="success" size="sm">
                      Get Details
                    </Button>
                  </Form.Group>
                </Form>
              </Col>
            </Row>
          </Card.Body>
        </Card>
        <br></br>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Date</th>
              <th>Case Id</th>
              <th>Patient Name</th>
              <th>Reference By</th>
              <th>Test</th>
              <th>Amount</th>
              <th>Paid</th>
              <th>Pending</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>12/08/2020</td>
              <td>101</td>
              <td>Mottu</td>
              <td>Dr.mdo</td>
              <td>Blood</td>
              <td>2500</td>
              <td>500</td>
              <td>2000</td>
            </tr>
            <tr>
              <td>18/08/2020</td>
              <td>102</td>
              <td>Patlu</td>
              <td>Dr.laku</td>
              <td>x-ray</td>
              <td>5500</td>
              <td>2000</td>
              <td>3500</td>
            </tr>
            <tr>
              <td>19/08/2020</td>
              <td>103</td>
              <td>test</td>
              <td>Dr.test</td>
              <td>Blood</td>
              <td>2500</td>
              <td>500</td>
              <td>2000</td>
            </tr>
            <tr>
              <td className="right" colSpan="5">
                Total
              </td>
              <td>8000</td>
              <td>2500</td>
              <td>5500</td>
            </tr>
          </tbody>
        </Table>
      </Container>
    </div>
  );
};
export default Accounts;
