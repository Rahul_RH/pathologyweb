import React, { Component } from 'react';
import { Button, Navbar, Form, NavDropdown, Nav, FormControl } from 'react-bootstrap';
import { Link, Switch, Route } from 'react-router-dom';
import Patient from '../Patient/patient';
// import { Login } from './../Login/login';
import { getUserDetails, destroyUserDetails } from '../../Services/localStorage.service';
import CaseRegistration from '../Case Registration/case_registration';
import Orders from '../Orders/orders';
import {BillPage} from '../Bill Page/bill_page';
import { CreateTestReport } from '../Create Test Report/create_test_report';
import { ViewReport } from '../View Report/view_report';
import { PrintReport } from '../Print Report/print_report';







class Dashboard extends Component {
    patientData;
    caseData;
    constructor(props) {
        super(props);
        // console.log(getUserDetails())
    }

    patient_Callback = (data) => {
        // console.log(data)
        this.patientData = data
        // this.props['patientData'] = 'test'
        this.props.history.push("/dashboard/caseregister");
    }

    caseReg_Callback = (data) => {
        this.caseData = data
        this.props.history.push("/dashboard/billpage");
    } 

    doLogout = () => {
        // console.log("test")
        destroyUserDetails()
        this.props.history.push("/")
    }

    render() {
        let { path } = this.props.match;

        return (
            <div>
                <Navbar bg="dark" variant="dark" expand="lg">
                    <Navbar.Brand >
                        <Link to={path} className="navbar-brand"> Dashboard </Link>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                            <Link to={`${path}/patient`} className="nav-link">Patient</Link>
                            <Link to={`${path}/orders`} className="nav-link">Orders</Link>
                            {/* <NavDropdown title="Others" id="basic-nav-dropdown">
                                <Link to={`${path}`} className="dropdown-item">Check Patient Status</Link>
                                <Link to={`${path}`} className="dropdown-item">Check Patient Report</Link>
                                <Link to={`${path}`} className="dropdown-item">Check Patient Payment</Link>
                                <NavDropdown.Divider />
                                <Link to={`${path}`} className="dropdown-item">Check Doctors Availability</Link>
                                
                            </NavDropdown> */}
                        </Nav>
                        {/* <Form inline>
                            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                            <Button variant="outline-success">Search</Button>
                        </Form> */}
                        <Button variant="outline-success" onClick={this.doLogout}>Logout</Button>
                    </Navbar.Collapse>
                </Navbar>
                <Switch>
                    <Route exact path={path} > Dashboard content 130820200700</Route>
                    <Route exact path={`${path}/patient`} render={(props) => <Patient {...props} callbackFromPatient={this.patient_Callback} />} />
                    <Route exact path='/dashboard/caseregister' render={props => 
                        <CaseRegistration {...props} 
                            patientData={this.patientData} 
                            callbackFromCaseReg={this.caseReg_Callback} />} />
                    <Route exact path={`${path}/orders`} component={Orders} />
                    <Route exact path={`${path}/billpage`} render={(props) => 
                        <BillPage {...props} 
                            caseData={this.caseData}/>} />
                    <Route exact path={`${path}/createreport`} component={CreateTestReport}/>  
                    <Route exact path={`${path}/viewreport`} component={ViewReport}/> 
                    
                    <Route exact path={`${path}/printreport`} component={PrintReport}/>
                        

                          
                </Switch>
                {/* <Login /> */}
            </div>
        );
    }
}

export default Dashboard;
