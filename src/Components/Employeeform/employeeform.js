import React from "react";
import { useHistory } from "react-router-dom";
import {
  Button,
  Table,
  Form,
  Row,
  Col,
  Card,
  Container,
} from "react-bootstrap";

const Employeeform = ({ path }) => {
  const { replace } = useHistory();

  const func = () => {
    replace(`/dashboard/users`);
  };
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const edit = urlParams.get("edit");

  return (
    <div>
      <Container>
        <Row className="justify-content-center">
          <Card className="text-center user-card">
            <Card.Header>Enter Employee Details</Card.Header>
            <Card.Body className="text-left">
              <Form>
                <Form.Group as={Col}>
                  <Form.Label>Employee Name</Form.Label>
                  <Form.Control
                    type="text"
                    required
                    placeholder="Enter Employee Name"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Label>Role</Form.Label>
                  <Form.Control type="text" required placeholder="Enter Role" />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Label>UserName</Form.Label>
                  <Form.Control
                    type="text"
                    required
                    placeholder="Enter UserName"
                  />
                </Form.Group>
                <Form.Group as={Col}>
                  <Form.Label>Password</Form.Label>
                  <Form.Control type="password" placeholder="Enter Password" />
                </Form.Group>
                <Button variant="primary" type="submit" onClick={() => func()}>
                  {edit === "update" ? "Update" : "Save"}
                </Button>
              </Form>
            </Card.Body>
          </Card>
        </Row>
      </Container>
    </div>
  );
};

export default Employeeform;
