import React from 'react';
import { ThemeProvider } from '@material-ui/core/styles';
import { Button, Card, Form, Container, Row } from 'react-bootstrap';
import './login.css'
import UserService from '../../Services/user.service';
// import { useHistory } from 'react-router-dom';



const theme = {
    colors: {
        powderWhite: "#FFFDF9",
        persianGreen: "#06B49A",
        lightBlue: "#AFDBD2",
        onyx: "#36313D"
    },
    fonts: ["sans-serif", "Roboto"],
    fontSizes: {
        small: "1em",
        medium: "2em",
        large: "3em"
    }
};
export class Login extends React.Component {

    userService = new UserService()
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            showPassword: 'password'
        }
    }

    performValidation = () => {
        return this.state.username.length > 0 && this.state.password.length > 0;
    }

    doLogin = (event) => {
        event.preventDefault();
        // console.log(this.state)

        this.userService.userLogin({
            "username": this.state.username, //"superadmin@pathology.in"
            "password": this.state.password //"Password@123"
        }).then(res => {
            if (res.code === 200) {
                this.props.history.push("/dashboard");
                console.log(res)
            }

        }).catch(err => alert(err.message))

    }
    showPassword = (event) => {
        // console.log(event.target.checked)
        if (event.target.checked) {
            this.setState({ showPassword: 'text' })
        } else {
            this.setState({ showPassword: 'password' })
        }
    }

    gotoRegister = () => {
        this.props.history.push("/register")
    }

    render() {
        return (
            <div>
                <ThemeProvider theme={theme}>
                    <Container >
                        <Row className="justify-content-center">
                            <Card className="text-center cust-card">
                                <Card.Header>Welcome to Pathology</Card.Header>
                                <Card.Body className="text-left">
                                    {/* <Card.Title>Special title treatment</Card.Title> */}
                                    <Form onSubmit={this.doLogin}>
                                        <Form.Group >
                                            <Form.Label>Email address</Form.Label>
                                            <Form.Control type="email" placeholder="Enter email" onChange={(e) => this.setState({ username: e.target.value })} />
                                            <Form.Text className="text-muted">
                                                We'll never share your email with anyone else.
                                                </Form.Text>
                                        </Form.Group>

                                        <Form.Group >
                                            <Form.Label>Password</Form.Label>
                                            <Form.Control type={this.state.showPassword} placeholder="Password" onChange={(e) => this.setState({ password: e.target.value })} />
                                        </Form.Group>
                                        <Form.Group controlId="formBasicCheckbox">
                                            <Form.Check type="checkbox" label="Show Password" onChange={this.showPassword} />
                                        </Form.Group>
                                        <Row className="justify-content-around">
                                            <Button variant="primary" disabled={!this.performValidation()} type="submit">  Login </Button>
                                            {/* <Button variant="primary" onClick={this.gotoRegister}>  Register </Button> */}
                                        </Row>

                                    </Form>
                                </Card.Body>
                            </Card>
                        </Row>
                    </Container>
                </ThemeProvider>
            </div>
        )
    }
}