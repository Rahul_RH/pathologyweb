import React, { Component } from 'react'
import { Button, Container, Form, Col, Row, Card } from 'react-bootstrap';
import './case_registration.css'
import CaseRegistraionService from '../../Services/case_register.service';
import { getUserDetails } from '../../Services/localStorage.service';


class CaseRegistration extends Component {

    caseRegistraionService = new CaseRegistraionService()

    sl_no = 1
    constructor(props) {
        super(props);
        this.state = {
            'ref_by': "",
            'test_cases': [{
                sl_no: this.sl_no,
                test_id: "",
                qty: 1,
                discount : 0,
                actual_price: 0,
                final_price: 0
            }],
            "total": 0,
            "final_amt" : 0,
            "amt_payable": 0,
            "balance" : 0,
            "payment_mode": "",
            "payment_type": "",
            "discount" : 0,
            "discount_type" : true,
            "note": "",
            'test_items': [],
            'referral_list' : []
        };
        // console.log(props)
        this.getTestItemsLists()
        this.getReferralLists()
    }

    getTestItemsLists = () => {
        this.caseRegistraionService.getTestItems({ "pathology_id": getUserDetails().pathology_id.id }).then(res => {
            this.setState({ test_items: res.Response })
        }).catch(error => {
            alert(error.message)
        })
    }

    getReferralLists = () => {
        this.caseRegistraionService.getReferralList({ "pathology_id": getUserDetails().pathology_id.id }).then(res => {
            this.setState({ referral_list: res.Response })
        }).catch(error => {
            alert(error.message)
        })
    }

    addnewtest = () => {
        let addObj = [...this.state.test_cases, {
            sl_no: ++this.sl_no,
            test_id: "",
            qty: 1,
            discount : 0,
            actual_price: 0,
            final_price: 0
        }]
        this.setState({ test_cases: addObj })
        // this.state.test_cases = [1, 2, 3]
        // console.log(this.state.test_cases)
    }

    setTestId = (testId, index) => {
        this.state.test_cases.map((data, i) => {
            if (i === index) {
                data.test_id = testId
                let test = this.state.test_items.find(ele => ele.id === testId);
                data.actual_price = test.price * data.qty
                data.final_price = test.price * data.qty
                data.discount = 0
            }
            return data
        })
        let totalAmt = this.state.test_cases.reduce((prev, curr) => {
            return { sum: prev.sum + curr.final_price }
        }, { sum: 0 }).sum
        this.setState({
            test_cases: this.state.test_cases,
            total: totalAmt,
            final_amt : totalAmt,
            amt_payable: 0,
            discount_type : true,
            discount : 0
        })

    }

    setTestQty = (qty, index) => {
        this.state.test_cases.map((data, i) => {
            if (i === index) {
                data.qty = parseInt(qty) || 0
                let test = this.state.test_items.find(ele => ele.id === data.test_id);
                if (test) {
                    data.actual_price = test.price * qty
                    data.final_price = test.price * qty
                }
            }
            return data
        })
        let totalAmt = this.state.test_cases.reduce((prev, curr) => {
            return { sum: prev.sum + curr.final_price }
        }, { sum: 0 }).sum
        this.setState({
            test_cases: this.state.test_cases,
            total: totalAmt,
            final_amt : totalAmt,
            amt_payable: 0,
            discount_type : true,
            discount : 0
        })
    }

    setTestDiscount = (discount, index) => {
        console.log(discount)
        this.state.test_cases.map((data, i) => {
            if (i === index) {
                data.discount = parseInt(discount) || 0
                let test = this.state.test_items.find(ele => ele.id === data.test_id);
                if (test) {
                    data.final_price = test.price - test.price * (discount / 100 )
                }
            }
            return data
        })
        let totalAmt = this.state.test_cases.reduce((prev, curr) => {
            return { sum: prev.sum + curr.final_price }
        }, { sum: 0 }).sum
        this.setState({
            test_cases: this.state.test_cases,
            total: totalAmt,
            final_amt : totalAmt,
            amt_payable: 0,
            discount_type : true,
            discount : 0
        })
    }

    setActualPrice = (value, index) => {
        
        this.state.test_cases.map((data, i) => {
            if (i === index) {
                data.actual_price = parseInt(value) || 0
                // data.price = parseInt(value) || 0
                let test = this.state.test_items.find(ele => ele.id === data.test_id);
                if (test) {
                    test.price = data.actual_price
                    data.final_price = test.price - test.price * (data.discount / 100 )
                }
            }
            return data
        })
        console.log(this.state.test_cases)
        let totalAmt = this.state.test_cases.reduce((prev, curr) => {
            return { sum: prev.sum + curr.final_price }
        }, { sum: 0 }).sum
        this.setState({
            test_cases: this.state.test_cases,
            total: totalAmt,
            final_amt : totalAmt,
            amt_payable: 0,
            discount_type : true,
            discount : 0
        })
    }

    discountTypeChange = (e) => {
        // console.log(typeof e.target.value)
        // this.setState({discount_type : e.target.value === 'true'})
        let _discount = this.state.discount 
        let _finalAmt = this.state.final_amt
        // console.log(typeof this.state.discount_type)
        if(e.target.value === 'true') {
            _finalAmt =  this.state.total - _discount
        } else {
            _finalAmt = this.state.total - this.state.total * (_discount / 100)
        }
        this.setState({ discount: _discount,
             final_amt : _finalAmt, 
             amt_payable : 0,
             discount_type : e.target.value === 'true'})
    }

    discountChange = (e) => {
        let _discount = e.target.value 
        let _finalAmt
        // 
        if(this.state.discount_type) {
            _finalAmt =  this.state.total - _discount
        } else {
            _finalAmt = this.state.total - this.state.total * (_discount / 100)
        }
        this.setState({ discount: _discount, final_amt : _finalAmt, amt_payable : 0})
    }

    amtPayable = (e) => {
        if(this.state.final_amt >= e.target.value ) {
            this.setState({ amt_payable: e.target.value, balance : this.state.final_amt - e.target.value })
        }
       
    }

    createTest = (event) => {
        event.preventDefault();
        let testList = this.state.test_cases.map(data => {
            return {
                "test_item_id": data.test_id,
                // "quantity": data.qty,
                "actual_test_price":data.actual_price,
                // "final_price": data.final_price,
                // "case_status_id": 1,
                "discount" : data.discount,
                "discount_type" : false
            }
        })
        // console.log(testList)
        let obj = {
            "register_date": new Date(),
            "patient_id": this.props.patientData.id,
            "paid_amount": parseInt(this.state.amt_payable),
            "discount_type" : this.state.discount_type,
	        "discount": parseInt(this.state.discount),
            "created_user_id": getUserDetails().id,
            "test_items": testList,
            "pathology_id": getUserDetails().pathology_id.id,
            "payment_mode": this.state.payment_mode,
            "payment_type": parseInt(this.state.payment_type),
            "payment_date": new Date(),
            "referral_id": this.state.ref_by,
            "note" : this.state.note,
            "comment" : "",
            "new_test_items" : []
        }
        // console.log(obj)
        this.caseRegistraionService.createTest(obj).then(res => {
            console.log(res)
            if (res.code === 200) {
                alert(res.message)
                // this.props.history.push("/dashboard");
                this.props.callbackFromCaseReg(res.Response)
            }
        }).catch(error => {
            alert(error.message)
        })

    }
    render() {
        // console.log(this.state.discount_type)
        return (
            <div>
                <Container >
                    <Row className="justify-content-center">
                        <Card className="text-center patient-card">
                            <Card.Header>Test Registration</Card.Header>
                            <Card.Body className="text-left">
                                <Form onSubmit={this.createTest}>

                                    <Form.Row>
                                        <Form.Group as={Col} >
                                            <Form.Label>Patient Name</Form.Label>
                                            <Form.Control disabled type="text"
                                                defaultValue={this.props.patientData.first_name || ''} />
                                        </Form.Group>

                                        <Form.Group as={Col} >
                                            <Form.Label>Patient Id</Form.Label>
                                            <Form.Control disabled type="text"
                                                defaultValue={this.props.patientData.patient_id || ''} />
                                        </Form.Group>
                                        <Form.Group as={Col} >
                                            <Form.Label>Reffered By *</Form.Label>
                                            <Form.Control as="select" required
                                                value={this.state.ref_by || ''}
                                                onChange={(e) => this.setState({ ref_by: e.target.value })}>
                                                <option value=''>Select...</option>
                                                {this.state.referral_list.map((data, index) => {
                                                    return (
                                                        <option key={index} value={data.id}>{data.doctor_name}</option>         
                                                    )
                                                })}
                                                
                                                {/* <option value='1'>Doctor 1</option>
                                                <option value='2'>Doctor 2</option>
                                                <option value='3'>Doctor 3</option> */}
                                            </Form.Control>
                                        </Form.Group>

                                    </Form.Row>
                                    <div className="test-area">
                                        {this.state.test_cases.map((data, index) => {
                                            return (
                                                <Form.Row key={index}>
                                                    <Form.Group as={Col} >
                                                        {index === 0 && <Form.Label>Sl. No.</Form.Label>}
                                                        <Form.Control disabled type="number" 
                                                            defaultValue={data.sl_no || ''} />
                                                    </Form.Group>
                                                    <Form.Group as={Col} >
                                                        {index === 0 && <Form.Label>Particulars *</Form.Label>}
                                                        <Form.Control as="select" required
                                                            value={data.test_id || ''}
                                                            onChange={(e) => e.target.value && this.setTestId(e.target.value, index)}>
                                                            <option value="">Select...</option>
                                                            {this.state.test_items.map((testData, testIndex) => {
                                                                return (<option key={testIndex} value={testData.id}>{testData.test_name}</option>)
                                                            })}
                                                        </Form.Control>
                                                    </Form.Group>
                                                    <Form.Group as={Col} >
                                                        {index === 0 && <Form.Label>Unit</Form.Label>}
                                                        <Form.Control disabled type="number"
                                                            defaultValue={data.qty || ''}
                                                            onChange={(e) => this.setTestQty(e.target.value, index)} />
                                                    </Form.Group>

                                                    <Form.Group as={Col} >
                                                        {index === 0 && <Form.Label>Actual Price *</Form.Label>}
                                                        <Form.Control type="number" required
                                                            defaultValue={data.actual_price || ''}
                                                            onChange={(e) => this.setActualPrice(e.target.value, index)} />
                                                    </Form.Group>

                                                    <Form.Group as={Col} >
                                                        {index === 0 && <Form.Label>Discount %</Form.Label>}
                                                        <Form.Control type="number" min="0"
                                                            defaultValue={data.discount || ''}
                                                            onChange={(e) => this.setTestDiscount(e.target.value, index)} />
                                                    </Form.Group>

                                                    <Form.Group as={Col} >
                                                        {index === 0 && <Form.Label>Final Price</Form.Label>}
                                                        <Form.Control disabled type="number"
                                                            value={data.final_price}
                                                            onChange={(e) => e} />
                                                    </Form.Group>
                                                    {index === this.state.test_cases.length - 1 && <Button className="add-button" variant="primary" type="button" onClick={this.addnewtest}>Add</Button>}

                                                </Form.Row>
                                            )
                                        })}
                                    </div>
                                        <Form.Group as={Row} >
                                            <Form.Label column sm={2}>Total Amount</Form.Label>
                                            <Col sm={2}>
                                            <Form.Control type="number" disabled 
                                                value={this.state.total || ''}
                                                onChange={(e) => this.setState({ total: e.target.value })} />
                                            </Col>  
                                        </Form.Group>
                                        <fieldset>
                                            <Form.Group as={Row}>
                                                <Form.Label as="legend" column sm={2}>
                                                Discount Type
                                                </Form.Label>
                                                <Col sm={2}>
                                                <Form.Check type="radio" label="Flat" 
                                                name="formHorizontalRadios"
                                                id="formHorizontalRadios1"
                                                    value={true}
                                                    checked={this.state.discount_type}
                                                    onChange={this.discountTypeChange}
                                                />
                                                <Form.Check
                                                    type="radio"
                                                    label="Percentage" 
                                                    name="formHorizontalRadios"
                                                    id="formHorizontalRadios2"
                                                    checked={!this.state.discount_type}
                                                    value={false}
                                                    onChange={this.discountTypeChange}
                                                />
                                               
                                                </Col>
                                            </Form.Group>
                                        </fieldset>
                                        <Form.Group as={Row} >
                                            <Form.Label column sm={2}>Discount</Form.Label>
                                            <Col sm={2}>
                                            <Form.Control type="number" min="0"
                                                value={this.state.discount || ''}
                                                onChange={this.discountChange} />
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row} >
                                            <Form.Label column sm={2}>Amount To Pay</Form.Label>
                                            <Col sm={2}>
                                            <Form.Control type="number" disabled
                                                value={this.state.final_amt || ''}
                                                onChange={(e) => this.setState({ final_amt: e.target.value })} />
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row} >
                                            <Form.Label column sm={2}>Amount Paid *</Form.Label>
                                            <Col sm={2}>
                                            <Form.Control type="number" min="0" required
                                                value={this.state.amt_payable || ''}
                                                onChange={this.amtPayable} />
                                            </Col>
                                        </Form.Group>
                                        <Form.Group as={Row} >
                                            <Form.Label column sm={2}>Amount Balance</Form.Label>
                                            <Col sm={2}>
                                            <Form.Control type="number" disabled
                                                value={this.state.balance || '' }
                                                 />
                                            </Col>
                                        </Form.Group>


                                   
                                    <Form.Row>
                                        <Form.Group as={Col} >
                                            <Form.Label>Payment Mode *</Form.Label>
                                            <Form.Control as="select" required
                                                value={this.state.payment_mode || ''}
                                                onChange={(e) => this.setState({ payment_mode: e.target.value })}>
                                                <option value="">Select...</option>
                                                <option value='Single'>Single</option>
                                                <option value='Multiple'>Multiple</option>

                                            </Form.Control>
                                        </Form.Group>
                                        <Form.Group as={Col} >
                                            <Form.Label>Payment Type *</Form.Label>
                                            <Form.Control as="select" required
                                                value={this.state.payment_type || ''}
                                                onChange={(e) => this.setState({ payment_type: e.target.value })}>
                                                <option value="">Select...</option>
                                                <option value='1'>Cash</option>
                                                <option value='2'>Card</option>
                                                {/* <option value='3'>Doctor 3</option> */}
                                            </Form.Control>
                                        </Form.Group>

                                        <Form.Group as={Col} >
                                            <Form.Label>Note</Form.Label>
                                            <Form.Control type="text"
                                                value={this.state.note || ''}
                                                onChange={(e) => this.setState({ note: e.target.value })} />
                                        </Form.Group>


                                    </Form.Row>

                                    <Button variant="primary" type="submit">Submit</Button>
                                </Form>
                            </Card.Body>
                        </Card>
                    </Row>
                </Container>

            </div>
        );
    }
}

export default CaseRegistration;