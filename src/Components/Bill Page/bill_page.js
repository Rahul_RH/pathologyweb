
import React, { Component } from 'react';
import ReactToPrint from 'react-to-print';
import './bill_page.css'
import { Button, Table, Row, Col, Container } from 'react-bootstrap';
import CaseRegistraionService from '../../Services/case_register.service';


export class BillPage extends Component {

  caseId;
    constructor(props) {
      super(props)
     this.caseId = props.caseData.id
    //  this.caseId = '25551c38-330a-4d77-b453-aab380b0512f'
    }

  closePage = () => {
    // console.log("test")
    this.props.history.push("/dashboard");
  }
  render() {
    return (
      <div>
        <BillTemplate {...this.props} caseId={this.caseId} ref={el => (this.componentRef = el)} />
        <ReactToPrint
          trigger={() => {
            // NOTE: could just as easily return <SomeComponent />. Do NOT pass an `onClick` prop
            // to the root node of the returned component as it will be overwritten.
            return <Button>Print</Button>
          }
        }
          content={() => this.componentRef}
        />
        <Button onClick={this.closePage}>Close</Button>
        
      </div>
    );
  }
}

class BillTemplate extends Component {
      _isMounted = false;
      caseRegistraionService = new CaseRegistraionService()
      constructor(props) {
        super(props)
        this.state = {
          caseDetails : {}
        }
        console.log("billpage", props)
      }
      componentDidMount() {
        this._isMounted = true;
        let obj = {
          'id' : this.props.caseId
        }
        this.caseRegistraionService.getCaseById(obj).then(res => {
          if(res.code === 200) {
            console.log(res.Response)
            if (this._isMounted) {
            this.setState({caseDetails : res.Response})
            }
          }
        }).catch(err => {
          alert(err.message)
        })
        
    }
    componentWillUnmount() {
        this._isMounted = false;
      }
    render() {
      return (
        <div> {Object.keys(this.state.caseDetails).length > 0 && 
        
        <div className="body-padding">
          
          <div className="bill-header">
          <img src={this.state.caseDetails.pathology_id.logo_name} width={100+'px'}/>
            <div>{this.state.caseDetails.pathology_id.pathology_name || ''} </div>
            <div>{this.state.caseDetails.pathology_id.address || ''} </div>
            <div>{this.state.caseDetails.pathology_id.email_id || ''} </div>
            <div>{this.state.caseDetails.pathology_id.phone_number || ''} </div>
            <div>GSTIN</div>
          </div>

          <div className='inner-body'>
          <div>
            <div className="heading"> Receipt </div> 
            <div className='flex-align'>
              <div>Case ID: {this.state.caseDetails.case_no || ''} </div>
              <div>Bill-No:</div>
            </div>
            <div className='flex-align'>
              <div>Ref Doctor: {this.state.caseDetails.referral_id.doctor_name || ''}  {this.state.caseDetails.note && ''+this.state.caseDetails.note+'' } </div>
              <div>Date: {new Date(this.state.caseDetails.register_date || '').toLocaleDateString()} </div>
            </div>
          </div>
          <div className='patient-data'>
            <Container>
              <Row>
                <Col xs={3}>Name <br/>{this.state.caseDetails.patient_id.first_name || ''} {this.state.caseDetails.patient_id.last_name || ''}</Col>
                <Col>ID <br/>{this.state.caseDetails.patient_id.patient_id || ''}</Col>
                <Col xs={3}>Age <br/>{this.state.caseDetails.patient_id.age && this.state.caseDetails.patient_id.age+' Years ' || ''}
                {this.state.caseDetails.patient_id.months && this.state.caseDetails.patient_id.months+' Months ' || ''}
                {this.state.caseDetails.patient_id.days && this.state.caseDetails.patient_id.days+' Days ' || ''}</Col>
                <Col>Gender <br/>{this.state.caseDetails.patient_id.gender.gender_name || ''}</Col>
                <Col>Mobile <br/>{this.state.caseDetails.patient_id.patient_contact_no || ''}</Col>
              </Row>
            </Container>
          </div>
          <div>Payment Type:</div>
          <Table bordered  className="item-table" size="sm">
            <thead>
              <tr>
                <th>Sl.No</th>
                <th>Particulars</th>
                <th>Qty</th>
                <th>Rate</th>
                <th>Amount</th>
              </tr>
            </thead>
            <tbody>
              {this.state.caseDetails.case_test_map.map((data, index) => {
                if(data.actual_test_price && data.final_test_price) {
                  return (
                    // {data.actual_test_price && data.final_test_price &&}
                    <tr key={index}>
                      <td>{index+1}</td>
                      <td>{data.test_item_id.test_name || ''}</td>
                      <td>1</td>
                      <td>{data.actual_test_price || ''}</td>
                      <th>{data.final_test_price || ''}</th>
                    </tr>
                  )
                }
                
              })}
             
            </tbody>
          </Table>
          </div>
          <div className='flex-align bold-text'  >
            <div >
              <div>Total Amount : </div>
              <div>Discount : </div>
              <div>Amount To Pay : </div>
              <div>Amount Paid : </div>
              <div>Amount Balance : </div>

            </div>
            <div style={{'minWidth': 100+'px'}}>
            <div>{this.state.caseDetails.actual_price || 0}</div>
              <div>{this.state.caseDetails.discount || 0}</div>
              <div>{this.state.caseDetails.final_price || 0}</div>
              <div>{ this.state.caseDetails.paid_amount || 0}</div>
              <div>{this.state.caseDetails.final_price - this.state.caseDetails.paid_amount || 0}</div>
            </div>
          </div>

          <div className="token-part">
           
          <img src={this.state.caseDetails.pathology_id.logo_name} width={100+'px'}/>
             <div className="token-head"> Patient Details </div>
            <div className='patient-data'>
              
            <Container>
              <Row>
                <Col xs={3}>Name <br/>{this.state.caseDetails.patient_id.first_name || ''} {this.state.caseDetails.patient_id.last_name || ''}</Col>
                <Col xs={1} >ID <br/>{this.state.caseDetails.patient_id.patient_id || ''}</Col>
                <Col xs={2}>Age <br/>{this.state.caseDetails.patient_id.age && this.state.caseDetails.patient_id.age+' Years ' || ''}
                {this.state.caseDetails.patient_id.months && this.state.caseDetails.patient_id.months+' Months ' || ''}
                {this.state.caseDetails.patient_id.days && this.state.caseDetails.patient_id.days+' Days ' || ''}</Col>
                <Col xs={1}>Gender <br/>{this.state.caseDetails.patient_id.gender.gender_name || 'female'}</Col>
                <Col xs={1}>Mobile <br/>{this.state.caseDetails.patient_id.patient_contact_no || ''}</Col>
                <Col xs={1}>Case ID <br/>{this.state.caseDetails.case_no || ''}</Col>
                <Col xs={3}>Referred By <br/>{this.state.caseDetails.referral_id.doctor_name || ''} {this.state.caseDetails.note && '('+this.state.caseDetails.note+')' }</Col>
              </Row>
            </Container>
          </div>
          <div className="token-head"> Tests </div>
            <Table bordered  className="item-table" >
              <thead>
                <tr>
                  <th>Sl.No</th>
                  <th>Particulars</th>
                  <th>Remark</th>
                  {/* <th></th>
                  <th>Amount</th> */}
                </tr>
              </thead>
              <tbody>
                {this.state.caseDetails.case_test_map.map((data, index) => {
                  if(data.actual_test_price && data.final_test_price) {
                    return (
                      // {data.actual_test_price && data.final_test_price &&}
                      <tr key={index}>
                        <td>{index+1}</td>
                        <td>{data.test_item_id.test_name || ''}</td>
                        <td>

                        </td>
                        {/* <td>{data.actual_test_price || ''}</td>
                        <th>{data.final_test_price || ''}</th> */}
                      </tr>
                    )
                  }
                })}
              </tbody>
            </Table>
          </div>

          
          
          
          </div> }
        </div>
        
      );
    }
  }