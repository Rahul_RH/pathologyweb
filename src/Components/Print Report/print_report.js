
import React, { Component } from 'react';
import ReactToPrint from 'react-to-print';
import './print_report.css'
import { Button, Table, Form, Row, Col } from 'react-bootstrap';
import CaseRegistraionService from '../../Services/case_register.service';
import { getTestReport } from '../../Services/localStorage.service';
import TestReportService from "../../Services/test_report.service";
import { getRangeTypeList, getUnitsList } from "../../Services/app_data.service";

export class PrintReport extends Component {

    testID;

    constructor(props) {
      super(props)
    //  this.caseId = props.caseData.id
    console.log(props)
    
        // console.log(allReports)
        this.testID = this.props.location.state.response.test_item_id.id
        
    }

  closePage = () => {
    // console.log("test")
    this.props.history.push("/dashboard");
  }
  render() {
    return (
      <div>
        <ReportTemplate {...this.props} testID={this.testID} caseID={this.props.location.state.response.case_id.id} ref={el => (this.componentRef = el)} />
        <ReactToPrint
          trigger={() => {
            // NOTE: could just as easily return <SomeComponent />. Do NOT pass an `onClick` prop
            // to the root node of the returned component as it will be overwritten.
            return <Button>Print</Button>
          }
        }
          content={() => this.componentRef}
        />
        <Button onClick={this.closePage}>Close</Button>
        
      </div>
    );
  }
}

class ReportTemplate extends Component {
      _isMounted = false;
      caseRegistraionService = new CaseRegistraionService()
      testReportService = new TestReportService()
      reportDetails;
      constructor(props) {
        super(props)
        this.state = {
            // reportDetails : {},
            caseDetails : {},
            testDetails: {},
            reportItems: [],
            showTable : false,
            myContent: `<div style="text-align:left">
      <b>
       
        LH:-
      </b>
    </div>
    <Table border="3">
      <thead>
        <tr>
          <th>REFERENCE GROUP</th>
          <th>REFERENCE RANGE IN mIU/mL</th>
        </tr>
      </thead>

      <tr>
        <td>
          <b>FEMALES</b>
        </td>
        <td></td>
      </tr>
      <tr>
        <td>* FOLLICULAR PHASE</td>
        <td>2.1 – 11.0</td>
      </tr>
      <tr>
        <td>MID CYCLE PEAK </td>
        <td>19.2 – 103</td>
      </tr>
      <tr>
        <td>LUTEAL PHASE </td>
        <td>1.2 – 12.8</td>
      </tr>
      <tr>
        <td>PREGNANCY </td>
        <td> 1.5</td>
      </tr>
      <tr>
        <td>POST MENOPAUSAL </td>
        <td>10.8 – 58.6</td>
      </tr>
      <tr>
        <td>
          <b>MALES</b>
        </td>
        <td></td>
      </tr>
    </Table>
    <div>
      Abnormal LH levels are interpreted with increased or decreased levels
      of other fertility hormones such as FSH, estrogens, progesterone, and
      testosterone. Increased LH levels are associated primary ovarian
      hypogonadism and gonadotropin secreting pituitary tumors. Decreased LH
      levels are associated with Hypothalamic GnRH deficiency, Pituitary LH
      deficiency, Ectopic steroid hormone production, GnRH analog treatment.
    </div>`,
        }
        console.log("billpage", props)
      }
      componentDidMount() {
        this._isMounted = true;
        let obj = {
          case_test_map_id: this.props.location.state.response.id,
        };
        console.log(this.props.location.state.response)
        this.testReportService.getReportParameters(obj).then(res => {
          if (this._isMounted) {
    
            let temp = res.Response.childTestItems.sort((a, b) => a.test_item_id.display_order - b.test_item_id.display_order)
            console.log(res.Response)
            if (res.Response.childTestItems.length > 0) {
              this.setState({ testDetails: res.Response, reportItems: temp });
            } else {
              this.setState({ testDetails: res.Response, reportItems: [res.Response] })
            }
          }
    
        }).catch(err => {
          alert(err.message)
        })
        // let allReports = getTestReport()
        // if(allReports && allReports.length > 0) {
        //     let temp = allReports.filter(data => data.id === this.props.location.state.response.test_item_id.id)
        //     this.reportDetails = temp[temp.length - 1]
        //     console.log(this.reportDetails)
        //     let _showTable = 0;
        //     this.reportDetails.subLevel.forEach(ele => {
        //       if(ele.range_type_id && ele.range_type_id.id <= 4) {
        //         _showTable++
        //       }
        //     })
        //     if(_showTable > 0) {
        //       this.setState({showTable : true})
        //     }
        //     this.setState({reportDetails : this.reportDetails})
        // }
        // let obj = {
        //   'id' : this.props.caseID
        // }
        // this.caseRegistraionService.getCaseById(obj).then(res => {
        //   if(res.code === 200) {
        //     console.log(res.Response)
        //     if (this._isMounted) {
        //     this.setState({caseDetails : res.Response})
        //     }
        //   }
        // }).catch(err => {
        //   alert(err.message)
        // })
        
    }
    componentWillUnmount() {
        this._isMounted = false;
      }

      getRangeType = (id) => {
        switch(id) {
          case 1 : return '-';
          case 2 : return '<';
          case 3 : return '>';
          case 4 : return '';
          default : return; 
        }
      }

    render() {
      return (
        <div> {
          // Object.keys(this.state.caseDetails).length > 0 && 
        
            <div className="outer-body">
              <div className="bill-header">
              </div>
              <div className='inner-body'>
             
             <Table striped bordered>
               <thead>
               <tr>
            <td className="text-left">
              Patient Name : {'this.state.caseDetails.patient_id.first_name' || ''} {'this.state.caseDetails.patient_id.last_name' || ''}<br></br>
              Age/Gender : {'this.state.caseDetails.patient_id.age'+' Years ' || ''}
                                                {/* {this.state.caseDetails.patient_id.months && this.state.caseDetails.patient_id.months+' Months ' || ''}
                                                {this.state.caseDetails.patient_id.days && this.state.caseDetails.patient_id.days+' Days' || ''}
              
              / {this.state.caseDetails.patient_id.gender.gender_name || ''}<br></br> */}
              Case ID : {'this.state.caseDetails.case_no'}<br></br>
              Patient ID : {'this.state.caseDetails.patient_id.patient_id' || ''} <br></br>
              Ref Doctor : {'this.state.caseDetails.referral_id.doctor_name' || ''} <br></br>
            </td>
            <td className="text">
              Registered : {new Date().toLocaleDateString()}<br></br>
              Reported : {new Date().toLocaleString()} <br></br>
              Status : Final Report<br></br>
              Client Name : {'this.state.caseDetails.pathology_id.pathology_name' || ''}<br></br>
              {/* Patient location : {this.state.caseDetails.patient_id.area || ''}, {this.state.caseDetails.patient_id.city || ''}, {this.state.caseDetails.patient_id.state || ''} */}
            </td>
          </tr>
               </thead>
         
        </Table>
              <Table  bordered>
                                <thead>
                                <tr>
                                  <td colSpan="5" style={{ textAlign: "left", fontWeight: "bold" }}>
                                   {/* {this.state.reportDetails && this.state.reportDetails.test_name || ''} */}
                                  </td>
                                </tr> 
                                { this.state.showTable && 
                                <tr>
                                    <th>Test Name</th>
                                    <th>Result</th>
                                    <th>Bio. Ref. Range</th>
                                    <th>Unit</th>
                                    {/* <th>Method</th> */}
                                </tr>}
                                
                                </thead>
                                <tbody>
                                {this.state.reportItems && this.state.reportItems.map((data, index) => {
                                  if(data.range_type_id && data.range_type_id <= 4) {
                                    return (
                                      <tr key={index}>
                                          <td>{data.test_item_id.test_name}</td>
                                          <td>{data.reading}</td>
                                          <td>{data.min_range} &nbsp; 
                                              {this.getRangeType(data.range_type_id)} &nbsp;
                                              {data.max_range}</td>
                                          <td>{data.unit_id }</td>
                                          {/* <th>{data.remarks}</th> */}
                                      </tr>
                                      )
                                  } else if(data.range_type_id && data.range_type_id === 6) {
                                   
                                      return (
                                        <tr key={index}>
                                            <th colSpan="4" style={{textAlign:"left"}}>{data.test_name}</th>    
                                        </tr>
                                      
                                    )
                                  }
                                   
                                })}
                                
                                
                                </tbody>
                            </Table>
                           
              <div className="text-record-block"> 
              { this.state.reportItems.map((data, index) => {
                if(data.range_type_id && (data.range_type_id === 5 || data.range_type_id === 7)) {
                  return (
                  <Row key={index} className="text-records">
                    <Col xs={3}>{data.test_item_id.test_name} </Col>
                    <Col>{data.actual_value}</Col>
                  </Row>
                  )
                }
              })}
            
              
              { this.state.reportItems.map((data, index) => {
                return (
                  <div key={index} dangerouslySetInnerHTML={{ __html: data.test_item_id.test_information }} />
                )
              })}
              </div>
             
              </div>
              
              </div> }
              <b>*** End Of Report ***</b>
            </div>
        
      );
    }
  }