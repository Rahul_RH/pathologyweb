import React, { Component } from "react";
import CaseRegistraionService from "../../Services/case_register.service";
import { Button, Container, Form, Col, Row, Card, Table } from "react-bootstrap";
import "./create_test_report.css";
import { createTestReport, getTestReport, destroyReport, getUserDetails } from "../../Services/localStorage.service";
import TestReportService from "../../Services/test_report.service";
import { getRangeTypeList, getUnitsList } from "../../Services/app_data.service";


export class CreateTestReport extends Component {
  _isMounted = false;
  caseRegistraionService = new CaseRegistraionService();
  testReportService = new TestReportService()
  rangeTypeList = getRangeTypeList() || []
  unitTypeList = getUnitsList() || []
  constructor(props) {
    super(props);
    this.state = {
      testDetails: {},
      reportItems: [],
      newReportItems: []
      // reportItems
    };
    // destroyReport()
    // console.log(this.rangeTypeList)
    this.addNewTestRecord()
  }

  addNewTestRecord = () => {
    this.state.newReportItems.push({
      "test_name": "",
      // "parent_test_id": "894f1230-658f-4356-9dc8-8b21aba6a328",
      "reading": "",
      "range_type_id": 1,
      "unit": 1,
      // "department_id": this.state.testDetails.test_item_id.department_id,
      "min_range": "",
      "max_range": ""
    })
    if (this._isMounted) {
      this.setState({ newReportItems: this.state.newReportItems })
    }
  }

  componentDidMount() {
    this._isMounted = true;
    let obj = {
      case_test_map_id: this.props.location.state.response.id,
    };
    console.log(this.props.location.state.response)
    this.testReportService.getReportParameters(obj).then(res => {
      if (this._isMounted) {

        let temp = res.Response.childTestItems.sort((a, b) => a.test_item_id.display_order - b.test_item_id.display_order)
        console.log(res.Response)
        if (res.Response.childTestItems.length > 0) {
          this.setState({ testDetails: res.Response, reportItems: temp });
        } else {
          this.setState({ testDetails: res.Response, reportItems: [res.Response] })
        }
      }

    }).catch(err => {
      console.log(err)
    })
    // this.caseRegistraionService
    //   .getTestReportItems(obj)
    //   .then((res) => {
    //     if (this._isMounted) {
    //       if (res.Response.sub_level.length > 0) {
    //         let items = res.Response.sub_level.map((data) => {
    //           data["actual_value"] = 0;
    //           data["remarks"] = "";
    //           return data;
    //         });
    //         this.setState({ caseDetails: res.Response, reportItems: items });
    //       } else {
    //         res.Response["actual_value"] = 0;
    //         res.Response["remarks"] = "";
    //         this.setState({
    //           caseDetails: res.Response,
    //           reportItems: [res.Response],
    //         });
    //       }
    //     }
    //     // console.log(res, getTestReport());
    //     this.isReportCreated()

    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });
  }
  isReportCreated = () => {
    // let oldData = getTestReport()
    // if(oldData && oldData.length > 0) {
    //   oldData.forEach(element => {
    //       if(element.id === this.state.caseDetails.id) {
    //         console.log(element)
    //         this.setState({reportItems : element.subLevel, newReportItems : element.newReportItems || [{
    //           "test_name" : "",
    //           "test_value" : "",
    //           "test_range" : "",
    //           "test_unit" : "",
    //           "test_remarks" : ""
    //         }]})
    //       }
    //   });
    // }

  }

  submitReport = (event) => {

    event.preventDefault();
    let _reportItems = this.state.reportItems.map(data => {
      data.user_id = getUserDetails().id
      return data
    })
    let _newReportItems = this.state.newReportItems.filter(data =>
      data.test_name && data.test_name.trim() && data.reading && data.reading.trim())
      .map(data => {
        data.parent_test_id = this.state.testDetails.test_item_id.id
        data.department_id = this.state.testDetails.test_item_id.department_id
        return data
      })
    let obj = {
      "case_test_map_ids": _reportItems,
      "pathology_id": getUserDetails().pathology_id.id,
      "case_id": this.state.testDetails.case_id.id,
      "new_child_items": _newReportItems

    }
    this.testReportService.createReport(obj).then(res => {
      if (res.code === 200) {
        console.log(res)
        alert(res.message);
        this.props.history.goBack();
      }

    }).catch(err => {
      alert(err.message)
    })
    // let oldList = getTestReport();
    // let newRecords = this.state.newReportItems.filter(data => {
    //    if(data.test_name || data.test_value || data.test_range || data.test_unit || data.test_remarks) {
    //     return data
    //    } 
    // })

    // if (oldList) {
    //   oldList.push({
    //     ...this.state.caseDetails,
    //     subLevel: this.state.reportItems,
    //     newReportItems : this.state.newReportItems
    //   });
    // } else {
    //   oldList = [
    //     { ...this.state.caseDetails, subLevel: this.state.reportItems, newReportItems : this.state.newReportItems },
    //   ];
    // }
    // console.log(newRecords)
    // createTestReport(oldList);
    // alert("report added successfully");
    // this.props.history.goBack();
  };

  cancelReport = () => {
    this.props.history.goBack();
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  setValue = (value, index) => {
    this.state.reportItems.map((data, i) => {
      if (i === index) {
        data.actual_value = value;
      }
      return data;
    });
    this.setState({ reportItems: this.state.reportItems });
  };

  setRemark = (value, index) => {
    this.state.reportItems.map((data, i) => {
      if (i === index) {
        data.comment = value;
      }
      return data;
    });
    this.setState({ reportItems: this.state.reportItems });
  };

  setRangeType = (value, index) => {
    this.state.reportItems.map((data, i) => {
      if (i === index) {
        data.range_type_id = parseInt(value);
      }
      return data;
    });
    this.setState({ reportItems: this.state.reportItems });
  }

  setUnitId = (value, index) => {
    this.state.reportItems.map((data, i) => {
      if (i === index) {
        data.unit_id = parseInt(value);
      }
      return data;
    });
    this.setState({ reportItems: this.state.reportItems });
  }
  setRangeTypeNewRecord = (value, index) => {
    this.state.newReportItems.map((data, i) => {
      if (i === index) {
        data.range_type_id = parseInt(value);
      }
      return data;
    });
    this.setState({ newReportItems: this.state.newReportItems });
  }

  setUnitIdNewRecord = (value, index) => {
    this.state.newReportItems.map((data, i) => {
      if (i === index) {
        data.unit = parseInt(value);
      }
      return data;
    });
    this.setState({ newReportItems: this.state.newReportItems });
  }



  render() {
    // console.log(this.state.reportItems)
    return (
      <div>

        <Container >
          <Row className="justify-content-center">
            <Card className="text-center patient-card">
              <Card.Header>Test Report</Card.Header>
              <Card.Body className="text-left">
                <Row className="header-block">
                  <Col>
                    <div className="header-names">Patient</div>
                    <Row>
                      <Col>
                        <div>ID: {this.props.location.state.response.case_id.patient_id.patient_id}</div>
                        <div>Name/Gender:
                                                    {this.props.location.state.response.case_id.patient_id.first_name + ' '}
                          {this.props.location.state.response.case_id.patient_id.last_name}/
                                                    {this.props.location.state.response.case_id.patient_id.gender.gender_name || ''}</div>
                      </Col>
                      <Col>
                        <div>Age : {this.props.location.state.response.case_id.patient_id.age + ' Years ' || ''}
                          {this.props.location.state.response.case_id.patient_id.months && this.props.location.state.response.case_id.patient_id.months + ' Months ' || ''}
                          {this.props.location.state.response.case_id.patient_id.days && this.props.location.state.response.case_id.patient_id.days + ' Days' || ''}</div>
                        <div>Mobile: {this.props.location.state.response.case_id.patient_id.patient_contact_no}</div>
                      </Col>
                    </Row>
                  </Col>
                  <Col>
                    <div className="header-names">Referred by</div>
                    <Row>
                      <Col>
                        <div>Name: {this.props.location.state.response.case_id.referral_id.doctor_name}</div>
                        <div>Mobile: {''}</div>
                      </Col>
                      <Col>

                      </Col>
                    </Row>
                  </Col>

                </Row>
                <Form onSubmit={this.submitReport}>
                  <Table striped bordered>
                    <thead>
                      <tr>
                        <th></th>
                        <th></th>
                        <th colSpan={3}>Range</th>
                        <th></th>
                        {/* <th></th> */}
                      </tr>
                      <tr>
                        <th>Name</th>
                        <th>Value</th>
                        <th>Min</th>
                        <th>type</th>
                        <th>Max</th>
                        <th>Unit</th>
                        {/* <th>Remark</th> */}
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.reportItems.length > 0 && this.state.reportItems.map((data, index) => {
                        if (data.range_type_id && data.range_type_id <= 4) {

                          return (

                            <tr key={index}>
                              <td>{data.test_item_id.test_name}</td>
                              <td><Form.Control type="text"
                                defaultValue={data.reading || ''}
                                onChange={(e) => data.reading = e.target.value} />
                              </td>
                              <td><Form.Control type="text"
                                defaultValue={data.min_range || ''}
                                onChange={(e) => data.min_range = e.target.value} />
                              </td>
                              <td><Form.Control as="select"
                                value={data.range_type_id || ''}
                                onChange={(e) => this.setRangeType(e.target.value, index)}>
                                {/* <option value=''>Select...</option> */}
                                {this.rangeTypeList.map((data, index) => {
                                  return (
                                    <option key={index} value={data.id}>{data.type_name}</option>
                                  )
                                })}
                              </Form.Control>
                              </td>
                              <td><Form.Control type="text"
                                defaultValue={data.max_range || ''}
                                onChange={(e) => data.max_range = e.target.value} /></td>
                              <td>
                                <Form.Control as="select"
                                  value={data.unit_id || ''}
                                  onChange={(e) => this.setUnitId(e.target.value, index)}>
                                  {/* <option value=''>Select...</option> */}
                                  {this.unitTypeList.map((data, index) => {
                                    return (
                                      <option key={index} value={data.id}>{data.unit_name}</option>
                                    )
                                  })}
                                </Form.Control></td>
                              {/* <td><Form.Control  type="text"
                                                defaultValue={data.comment || ''} 
                                                onChange={(e) => this.setRemark(e.target.value, index)}/></td> */}
                            </tr>
                          )
                        } else if (data.range_type_id && data.range_type_id === 7) {
                          return (
                            <tr key={index}>
                              <td>{data.test_item_id.test_name} 7777</td>
                              <td colSpan="4"><Form.Control type="text"
                                defaultValue={data.reading || ''}
                                onChange={(e) => this.setValue(e.target.value, index)} />
                              </td>
                              <td><Form.Control as="select"
                                value={data.range_type_id || ''}
                                onChange={(e) => this.setRangeType(e.target.value, index)}>
                                {/* <option value=''>Select...</option> */}
                                {this.rangeTypeList.map((data, index) => {
                                  return (
                                    <option key={index} value={data.id}>{data.type_name}</option>
                                  )
                                })}
                              </Form.Control>
                              </td>
                            </tr>
                          )
                        } else if (data.range_type_id && data.range_type_id === 5) {
                          return (
                            <tr key={index}>
                              <td>{data.test_item_id.test_name} 555</td>
                              <td colSpan="4"><textarea className="form-control" rows="3"
                                defaultValue={data.reading || ''}
                                onChange={(e) => this.setValue(e.target.value, index)}></textarea>
                              </td>
                              <td><Form.Control as="select"
                                value={data.range_type_id || ''}
                                onChange={(e) => this.setRangeType(e.target.value, index)}>
                                {/* <option value=''>Select...</option> */}
                                {this.rangeTypeList.map((data, index) => {
                                  return (
                                    <option key={index} value={data.id}>{data.type_name}</option>
                                  )
                                })}
                              </Form.Control>
                              </td>
                            </tr>
                          )
                        } else {
                          return (
                            <tr key={index}>
                              <td>{data.test_item_id.test_name} 1111</td>

                            </tr>
                          )
                        }

                      })}
                      {this.state.newReportItems.map((data, index) => {
                        if (data.range_type_id && data.range_type_id <= 4) {
                          return (
                            <tr key={index}>
                              <td> <Form.Control type="text"
                                defaultValue={data.test_name || ''}
                                onChange={(e) => data.test_name = e.target.value} /></td>
                              <td >
                                <Form.Control type="text"
                                  defaultValue={data.reading || ''}
                                  onChange={(e) => data.reading = e.target.value} />
                              </td>
                              <td><Form.Control type="text"
                                defaultValue={data.min_range || ''}
                                onChange={(e) => data.min_range = e.target.value} />
                              </td>
                              <td><Form.Control as="select"
                                value={data.range_type_id || ''}
                                onChange={(e) => this.setRangeTypeNewRecord(e.target.value, index)}>
                                {/* <option value=''>Select...</option> */}
                                {this.rangeTypeList.map((data, index) => {
                                  return (
                                    <option key={index} value={data.id}>{data.type_name}</option>
                                  )
                                })}
                              </Form.Control>
                              </td>
                              <td><Form.Control type="text"
                                defaultValue={data.max_range || ''}
                                onChange={(e) => data.max_range = e.target.value} /></td>
                              <td>
                                <Form.Control as="select"
                                  value={data.unit || ''}
                                  onChange={(e) => this.setUnitIdNewRecord(e.target.value, index)}>
                                  {/* <option value=''>Select...</option> */}
                                  {this.unitTypeList.map((data, index) => {
                                    return (
                                      <option key={index} value={data.id}>{data.unit_name}</option>
                                    )
                                  })}
                                </Form.Control></td>

                            </tr>
                          )
                        } else if (data.range_type_id && data.range_type_id === 7) {
                          return (
                            <tr key={index}>
                              <td> <Form.Control type="text"
                                defaultValue={data.test_name || ''}
                                onChange={(e) => data.test_name = e.target.value} /></td>
                              <td colSpan="4"><Form.Control type="text"
                                defaultValue={data.reading || ''}
                                onChange={(e) => data.reading = e.target.value} />
                              </td>
                              <td><Form.Control as="select"
                                value={data.range_type_id || ''}
                                onChange={(e) => this.setRangeTypeNewRecord(e.target.value, index)}>
                                {/* <option value=''>Select...</option> */}
                                {this.rangeTypeList.map((data, index) => {
                                  return (
                                    <option key={index} value={data.id}>{data.type_name}</option>
                                  )
                                })}
                              </Form.Control>
                              </td>
                            </tr>
                          )
                        } else if (data.range_type_id && data.range_type_id === 5) {
                          return (
                            <tr key={index}>
                              <td> <Form.Control type="text"
                                defaultValue={data.test_name || ''}
                                onChange={(e) => data.test_name = e.target.value} /></td>
                              <td colSpan="4"><textarea className="form-control" rows="3"
                                defaultValue={data.reading || ''}
                                onChange={(e) => data.reading = e.target.value}></textarea>
                              </td>
                              <td><Form.Control as="select"
                                value={data.range_type_id || ''}
                                onChange={(e) => this.setRangeTypeNewRecord(e.target.value, index)}>
                                {/* <option value=''>Select...</option> */}
                                {this.rangeTypeList.map((data, index) => {
                                  return (
                                    <option key={index} value={data.id}>{data.type_name}</option>
                                  )
                                })}
                              </Form.Control>
                              </td>
                            </tr>
                          )
                        } else {
                          return (
                            <tr key={index}>
                              <td>{data.test_name} </td>

                            </tr>
                          )
                        }

                      })}

                    </tbody>
                  </Table>

                  <Button variant="primary" type="button" onClick={this.addNewTestRecord}>Add new row</Button>
                  <Button variant="primary" type="submit">Submit</Button>
                  <Button variant="primary" type="button" onClick={this.cancelReport}>Cancel</Button>
                </Form>
              </Card.Body>
            </Card>
          </Row>
        </Container>
      </div>
    )
  }
}
