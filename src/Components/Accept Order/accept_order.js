import React, { Component } from "react";
import { Table, Button, Badge, Modal } from "react-bootstrap";
import "./accept_order.css";
import CaseRegistraionService from "../../Services/case_register.service";
import { getUserDetails } from "../../Services/localStorage.service";
import CollectSampleModal from "./collect_sample_modal";

class AcceptOrder extends Component {
  caseRegistraionService = new CaseRegistraionService();
  ordersList = [1, 2, 3];
  _isMounted = false;
  currentOrderData;
  actionStatus = "";
  modalHeader = "";
  constructor(props) {
    super(props);
    this.state = {
      ordersList: [],
      showModal: false,
      buttonName: "",
    };
    // this.getAllOrders()
    // console.log(props)
    this.actionStatus = props.status;
  }

  viewDetails = (data, index) => {
    let temp = this.state.ordersList;
    if (temp[index + 1]) {
      if (!temp[index + 1]["view"]) {
        temp.splice(index + 1, 0, { ...data, view: true });
        this.setState({ ordersList: temp });
      } else {
        temp.splice(index + 1, 1);
        this.setState({ ordersList: temp });
      }
    } else {
      temp.splice(index + 1, 0, { ...data, view: true });
      this.setState({ ordersList: temp });
    }
  };

  createReport = (data) => {
    this.props.history.push("/dashboard/createreport", { response: data });
  };

  viewReport = (data) => {
    this.props.history.push("/dashboard/viewreport", { response: data });
  };

  printReport = (data) => {
    this.props.history.push("/dashboard/printreport", { response: data });
  };

  componentDidMount() {
    this._isMounted = true;

    let obj = {
      pathology_id: getUserDetails().pathology_id.id,
    };
    if (this.props && this.props.status === "getSamplePending") {
      this.caseRegistraionService
        .getAllPendingTests(obj)
        .then((res) => {
          // console.log(res)
          res.Response.sort((a, b) => a.case_id.case_no - b.case_id.case_no)
          if (this._isMounted) {
            this.setState({
              ordersList: res.Response,
              buttonName: "Collect Sample",
            });
          }
        })
        .catch((err) => {
          alert(err.message);
        });
    } else if (this.props && this.props.status === "getSampleReceived") {
      this.caseRegistraionService
        .getAllSampleCollectedTests(obj)
        .then((res) => {
          res.Response.sort((a, b) => a.case_id.case_no - b.case_id.case_no)
          if (this._isMounted) {
            this.setState({
              ordersList: res.Response,
              buttonName: "Accept Sample",
            });
          }
        })
        .catch((err) => {
          alert(err.message);
        });
    } else if (this.props && this.props.status === "getTestCanceled") {
      this.caseRegistraionService
        .getCasesByTestCancelledStatus(obj)
        .then((res) => {
          res.Response.sort((a, b) => a.case_id.case_no - b.case_id.case_no)
          if (this._isMounted) {
            this.setState({
              ordersList: res.Response,
              buttonName: "Send to Lab",
            });
          }
        })
        .catch((err) => {
          alert(err.message);
        });
    } else if (this.props && this.props.status === "getSampleRejected") {
      this.caseRegistraionService
        .getCasesBySampleRejectedStatus(obj)
        .then((res) => {
          res.Response.sort((a, b) => a.case_id.case_no - b.case_id.case_no)
          if (this._isMounted) {
            this.setState({
              ordersList: res.Response,
              buttonName: "Collect Sample",
            });
          }
        })
        .catch((err) => {
          alert(err.message);
        });
    } else if (this.props && this.props.status === "getSampleForward") {
      this.caseRegistraionService
        .getCasesBySampleForwardStatus(obj)
        .then((res) => {
          res.Response.sort((a, b) => a.case_id.case_no - b.case_id.case_no)
          if (this._isMounted) {
            this.setState({
              ordersList: res.Response,
              buttonName: "Start Process",
            });
          }
        })
        .catch((err) => {
          alert(err.message);
        });
    } else if (this.props && this.props.status === "getLabReceived") {
      this.caseRegistraionService
        .getCasesByLabReceivedStatus(obj)
        .then((res) => {
          res.Response.sort((a, b) => a.case_id.case_no - b.case_id.case_no)
          if (this._isMounted) {
            this.setState({
              ordersList: res.Response,
              buttonName: "Start Process",
            });
          }
        })
        .catch((err) => {
          alert(err.message);
        });
    } else if (this.props && this.props.status === "getTestIncomplete") {
      this.caseRegistraionService
        .getCasesByTestIncompleteStatus(obj)
        .then((res) => {
          res.Response.sort((a, b) => a.case_id.case_no - b.case_id.case_no)
          if (this._isMounted) {
            this.setState({
              ordersList: res.Response,
              buttonName: "Complete Test",
            });
          }
        })
        .catch((err) => {
          alert(err.message);
        });
    } else if (this.props && this.props.status === "getTestReject") {
      this.caseRegistraionService
        .getCasesByTestRejectStatus(obj)
        .then((res) => {
          res.Response.sort((a, b) => a.case_id.case_no - b.case_id.case_no)
          if (this._isMounted) {
            this.setState({
              ordersList: res.Response,
              buttonName: "Collect Sample",
            });
          }
        })
        .catch((err) => {
          alert(err.message);
        });
    } else if (this.props && this.props.status === "getTestApproved") {
      this.caseRegistraionService
        .getCasesByTestApprovedStatus(obj)
        .then((res) => {
          res.Response.sort((a, b) => a.case_id.case_no - b.case_id.case_no)
          if (this._isMounted) {
            this.setState({
              ordersList: res.Response,
              buttonName: "Complete Test",
            });
          }
        })
        .catch((err) => {
          alert(err.message);
        });
    } else if (this.props && this.props.status === "getTestHold") {
      this.caseRegistraionService
        .getCasesByTestHoldStatus(obj)
        .then((res) => {
          res.Response.sort((a, b) => a.case_id.case_no - b.case_id.case_no)
          if (this._isMounted) {
            this.setState({
              ordersList: res.Response,
              buttonName: "Collect Sample",
            });
          }
        })
        .catch((err) => {
          alert(err.message);
        });
    } else if (this.props && this.props.status === "getTestComplete") {
      this.caseRegistraionService
        .getCasesByTestCompleteStatus(obj)
        .then((res) => {
          res.Response.sort((a, b) => a.case_id.case_no - b.case_id.case_no)
          if (this._isMounted) {
            this.setState({
              ordersList: res.Response,
              buttonName: "Approve Test",
            });
          }
        })
        .catch((err) => {
          alert(err.message);
        });
    } else if (this.props && this.props.status === "getCompleted") {
      this.caseRegistraionService
        .getCasesByCompletedStatus(obj)
        .then((res) => {
          res.Response.sort((a, b) => a.case_id.case_no - b.case_id.case_no)
          if (this._isMounted) {
            this.setState({
              ordersList: res.Response,
              buttonName: "Collect Sample",
            });
          }
        })
        .catch((err) => {
          alert(err.message);
        });
    }
  }

  dateFormat = (date) => {
    return new Date(date).toLocaleDateString();
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  onModalOpen = (_currentOrderData, _actionStatus, _modalHeader) => {
    this.currentOrderData = _currentOrderData;
    this.actionStatus = _actionStatus;
    this.modalHeader = _modalHeader;
    this.setState({ showModal: true });
  };

  onModalClose = (data) => {
    // console.log(data)
    this.setState({ showModal: false });
  };

  onModalSubmit = (modalData) => {
    // console.log(modalData)
    this.setState({ showModal: false });
    this.componentDidMount();
  };
  render() {
    return (
      <div className="page-body">
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Case No.</th>
              <th>Test Code</th>
              <th>Test Name</th>
              <th>Patient</th>
              <th>Referred By</th>
              <th>Register Date</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {this.state.ordersList.map((data, index) => {
              if (!data.view) {
                return (
                  <tr key={index}>
                    <td>{data.case_id.case_no}</td>
                    <td>{data.test_item_id.item_no}</td>
                    <td>{data.test_item_id.test_name}</td>

                    <td>
                      <div>
                        {" "}
                        {data.case_id.patient_id.first_name}{" "}
                        {data.case_id.patient_id.last_name}
                      </div>
                      {/* <div>Gender : {data.case_id.patient_id.gender.gender_name},  Age : {data.case_id.patient_id.age}</div>
                                            <div>Contact No. : {data.case_id.patient_id.patient_contact_no}</div> */}
                    </td>
                    <td> {data.case_id.referral_id.doctor_name} </td>
                    <td>
                      {new Date(data.case_id.register_date).toDateString()}
                    </td>
                    <td>
                      <Badge pill variant="danger">
                        {data.test_status_id.status_name}
                      </Badge>
                    </td>
                    <td>
                      {this.props.status === "getSamplePending" && (
                        <Button
                          variant="primary"
                          onClick={(e) =>
                            this.onModalOpen(
                              data,
                              "setSampleCollect",
                              "Collect Sample"
                            )
                          }
                        >
                          {" "}
                          Collect Sample
                        </Button>
                      )}
                      {this.props.status === "getSamplePending" && (
                        <Button
                          varient="primary"
                          onClick={(e) =>
                            this.onModalOpen(
                              data,
                              "setSampleForward",
                              "Forward Sample"
                            )
                          }
                        >
                          Forward Sample{" "}
                        </Button>
                      )}
                      {/* {this.props.status === 'getSamplePending' && <Button varient="primary" onClick={e => this.onModalOpen(data, 'setTestCancle', 'Cancle Test')}>Cancel Test </Button>} */}
                      {this.props.status === "getSampleReceived" && (
                        <Button
                          varient="primary"
                          onClick={(e) =>
                            this.onModalOpen(
                              data,
                              "setLabReceived",
                              "Send Sample To Lab"
                            )
                          }
                        >
                          Sample Recieved{" "}
                        </Button>
                      )}
                      {this.props.status === "getLabReceived" && (
                        <Button
                          varient="primary"
                          onClick={(e) =>
                            this.onModalOpen(
                              data,
                              "setTestComplete",
                              "Complete Test"
                            )
                          }
                        >
                          Complete Test{" "}
                        </Button>
                      )}
                      {this.props.status === "getLabReceived" && (
                        <Button
                          varient="primary"
                          onClick={(e) =>
                            this.onModalOpen(
                              data,
                              "setSampleRejected",
                              "Reject Sample"
                            )
                          }
                        >
                          Reject Sample{" "}
                        </Button>
                      )}
                      {this.props.status === "getTestComplete" && (
                        <Button
                          varient="primary"
                          onClick={(e) =>
                            this.onModalOpen(
                              data,
                              "setTestApproved",
                              "Approve Test"
                            )
                          }
                        >
                          Approve Test{" "}
                        </Button>
                      )}
                      {this.props.status === "getTestComplete" && (
                        <Button
                          varient="primary"
                          onClick={(e) =>
                            this.onModalOpen(data, "setTestHold", "Hold Test")
                          }
                        >
                          Hold Test{" "}
                        </Button>
                      )}
                      {(this.props.status === "getSampleRejected" ||
                        this.props.status === "getTestReject" ||
                        this.props.status === "getSamplePending") && (
                        <Button
                          varient="primary"
                          onClick={(e) =>
                            this.onModalOpen(
                              data,
                              "setTestCancle",
                              "Cancel Test"
                            )
                          }
                        >
                          Cancel Test{" "}
                        </Button>
                      )}
                      {(this.props.status === "getSampleRejected" ||
                        this.props.status === "getTestReject") && (
                        <Button
                          varient="primary"
                          onClick={(e) =>
                            this.onModalOpen(
                              data,
                              "setSamplePending",
                              "Collect Sample Again"
                            )
                          }
                        >
                          Collect Sample Again
                        </Button>
                      )}
                      {this.props.status === "getTestApproved" && (
                        <Button
                          varient="primary"
                          onClick={(e) =>
                            this.onModalOpen(data, "setCompleted", "Complete")
                          }
                        >
                          Complete{" "}
                        </Button>
                      )}
                      {this.props.status === "getTestHold" && (
                        <Button
                          varient="primary"
                          onClick={(e) =>
                            this.onModalOpen(
                              data,
                              "setTestReject",
                              "Reject Test"
                            )
                          }
                        >
                          Reject Test
                        </Button>
                      )}

                      {/* {this.props && this.props.status === 'samplePending' && <Button varient="primary" onClick={e => this.onModalOpen(data)}>Forward Sample </Button>}
                                            {this.props && this.props.status === 'samplePending' && <Button varient="primary" onClick={e => this.onModalOpen(data)}>Forward Sample </Button>} */}
                      {this.props && this.props.status === "getLabReceived" && (
                        <Button
                          varient="primary"
                          onClick={(e) => this.createReport(data)}
                        >
                          Create Report{" "}
                        </Button>
                      )}
                      {this.props && this.props.status === "getTestComplete" && (
                        <Button
                          varient="primary"
                          onClick={(e) => this.viewReport(data)}
                        >
                          View Report{" "}
                        </Button>
                      )}
                      {this.props && this.props.status === "getCompleted" && (
                        <Button
                          varient="primary"
                          onClick={(e) => this.printReport(data)}
                        >
                          Print Report{" "}
                        </Button>
                      )}
                    </td>
                  </tr>
                );
              } else {
                return (
                  <tr key={index}>
                    <td colSpan="5">
                      <div className="view-details">
                        <div>{data.test_item_id.test_name}</div>
                        <div>{data.test_item_id.item_no}</div>
                        <div>{this.dateFormat(data.created_at)}</div>

                        {this.props && this.props.status === "samplePending" && (
                          <Button
                            variant="primary"
                            onClick={(e) => this.onModalOpen(data)}
                          >
                            {this.state.buttonName}
                          </Button>
                        )}
                        {/* {this.props && this.props.status === 'testIncomplete' && <Button varient="primary" onClick={e => this.createReport(data)}>Create Report </Button>} */}
                      </div>
                    </td>
                  </tr>
                );
              }
            })}
          </tbody>
        </Table>

        {this.state.showModal && (
          <CollectSampleModal
            orderdata={this.currentOrderData}
            orderstatus={this.props.status}
            actionstatus={this.actionStatus}
            modalheader={this.modalHeader}
            show={this.state.showModal}
            onHide={() => this.onModalClose()}
            onSubmit={(res) => this.onModalSubmit(res)}
          />
        )}
      </div>
    );
  }
}

export default AcceptOrder;
