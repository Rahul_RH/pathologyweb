import React, { useState } from 'react';
import { Button, Modal, Table, Form, Col, Row } from 'react-bootstrap'
import './collect_sample_modal.css'
import CaseRegistraionService from '../../Services/case_register.service';
import { getUserDetails } from '../../Services/localStorage.service';


export default function CollectSampleModal(props) {
    const [comment, setComment] = useState('');
    // const handleClose = () => setShow(false);
    // const handleShow = () => setShow(true);
    const curDateTime = new Date()
    let modalheading = '';
    const caseRegistraionService = new CaseRegistraionService()
    console.log(props)

   

    // componentDidMount() {
    //     _isMounted = true;
    // }

    // componentWillUnmount() {
    //     _isMounted = false;
    // }

    const submit = () => {
        let obj = {
            "user_id":getUserDetails().id,
            "case_id": props.orderdata.case_id.id,
            "case_test_map_id": props.orderdata.id,
            "comment": comment,
            "reading" : 0.1
        }
        switch(props.actionstatus) {
            case 'setSampleCollect' : 
                    caseRegistraionService.sampleCollectedStatus(obj).then(res=> {
                        alert(res.message)
                        props.onSubmit("test")
                    }).catch(err => {
                        alert(err.message)
                    })
                    break;
            case 'setSampleForward' :
                    caseRegistraionService.sampleForwardStatus(obj).then(res=> {
                        alert(res.message)
                        props.onSubmit("test")
                    }).catch(err => {
                        alert(err.message)
                    })    
                    break;
            case 'setTestCancle' : 
                    caseRegistraionService.testCancelledStatus(obj).then(res=> {
                        alert(res.message)
                        props.onSubmit("test")
                    }).catch(err => {
                        alert(err.message)
                    })
                    break;
            case 'setLabReceived' : 
                    caseRegistraionService.labReceivedStatus(obj).then(res=> {
                        alert(res.message)
                        props.onSubmit("test")
                    }).catch(err => {
                        alert(err.message)
                    })
                    break;
            case 'setTestComplete' : 
                    caseRegistraionService.testCompleteStatus(obj).then(res=> {
                        alert(res.message)
                        props.onSubmit("test")
                    }).catch(err => {
                        alert(err.message)
                    })
                    break;
            case 'setSampleRejected' :
                    caseRegistraionService.sampleRejectedStatus(obj).then(res=> {
                        alert(res.message)
                        props.onSubmit("test")
                    }).catch(err => {
                        alert(err.message)
                    })
                    break;
            case 'setTestApproved' : 
                    caseRegistraionService.testApprovedStatus(obj).then(res=> {
                        alert(res.message)
                        props.onSubmit("test")
                    }).catch(err => {
                        alert(err.message)
                    })
                    break;
            case 'setTestHold' : 
                    caseRegistraionService.testHoldStatus(obj).then(res=> {
                        alert(res.message)
                        props.onSubmit("test")
                    }).catch(err => {
                        alert(err.message)
                    })
                    break;
            case 'setSamplePending' : 
                    caseRegistraionService.samplePendingStatus(obj).then(res=> {
                        alert(res.message)
                        props.onSubmit("test")
                    }).catch(err => {
                        alert(err.message)
                    })
                    break;
            case 'setCompleted': 
                    caseRegistraionService.completedStatus(obj).then(res=> {
                        alert(res.message)
                        props.onSubmit("test")
                    }).catch(err => {
                        alert(err.message)
                    })
                    break;
            case 'setTestReject': 
                    caseRegistraionService.testRejectStatus(obj).then(res=> {
                        alert(res.message)
                        props.onSubmit("test")
                    }).catch(err => {
                        alert(err.message)
                    })
                    break;        
            default : break;

        }
       
        
    }
    

    return (

        <div>

            <Modal {...props}
                show={props.show}
                onHide={props.onHide}
                backdrop="static"
                size="lg"
                keyboard={false}
                aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header closeButton>
                    <Modal.Title >{props.modalheader}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Table striped bordered hover >
                            <thead>
                                <tr>
                                    
                                    <th>Test Name</th>

                                    <th>Method </th>

                                </tr>
                            </thead>
                            <tbody>

                                {/* {props.orderdata && props.orderdata.case_test_map.map((data, index) => {
                                    return ( */}
                                        <tr >
                                           
                                            <td> {props.orderdata && props.orderdata.test_item_id.test_name}</td>

                                            <td>{props.orderdata && props.orderdata.test_item_id.method}</td>

                                        </tr>
                                    {/* )
                                })} */}

                            </tbody>
                        </Table>
                        <div className="time-date">
                            <div>Sample Date</div>
                            <div> {curDateTime.toLocaleDateString()}</div>
                            <div>Sample Time</div>
                            <div>{curDateTime.toLocaleTimeString()}</div>

                        </div>
                       
                        <Form.Group as={Col} >
                            <Form.Label>Phlebo Notes</Form.Label>
                            <Form.Control type="text" required
                                placeholder="Enter Note"
                                defaultValue={comment}
                                onChange={(e) => setComment(e.target.value)} />
                        </Form.Group>
                    </Form>

                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={props.onHide}>Close</Button>
                    <Button variant="primary" onClick={submit}>Submit</Button>
                </Modal.Footer>
            </Modal>
        </div>



    );
}

// render(<CollectSampleModal />);