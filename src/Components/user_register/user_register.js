import React, { Component } from 'react'
import { ThemeProvider } from '@material-ui/core/styles';
import { Button, Card, Form, Container, Row } from 'react-bootstrap';
import './user_register.css'


const theme = {
    colors: {
        powderWhite: "#FFFDF9",
        persianGreen: "#06B49A",
        lightBlue: "#AFDBD2",
        onyx: "#36313D"
    },
    fonts: ["sans-serif", "Roboto"],
    fontSizes: {
        small: "1em",
        medium: "2em",
        large: "3em"
    }
};
class UserRegister extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            showPassword: 'password'
        }
    }

    performValidation = () => {
        return this.state.username.length > 0 && this.state.password.length > 0;
    }

    doLogin = (event) => {
        event.preventDefault();
        // console.log(this.state)
        this.props.history.push("/dashboard");

    }
    showPassword = (event) => {
        // console.log(event.target.checked)
        if (event.target.checked) {
            this.setState({ showPassword: 'text' })
        } else {
            this.setState({ showPassword: 'password' })
        }
    }

    gotoRegister = () => {
        this.props.history.push("/register")
    }

    render() {
        return (
            <div>
                <ThemeProvider theme={theme}>
                    <Container >
                        <Row className="justify-content-center">
                            <Card className="text-center cust-card">
                                <Card.Header>Register User</Card.Header>
                                <Card.Body className="text-left">
                                    {/* <Card.Title>Special title treatment</Card.Title> */}
                                    <Form onSubmit={this.doLogin}>
                                        <Form.Group >
                                            <Form.Label>First Name</Form.Label>
                                            <Form.Control type="text" placeholder="Enter First Name" />
                                        </Form.Group>
                                        <Form.Group >
                                            <Form.Label>Last Name</Form.Label>
                                            <Form.Control type="text" placeholder="Enter Last Name" />
                                        </Form.Group>
                                        <Form.Group >
                                            <Form.Label>Contact Number</Form.Label>
                                            <Form.Control type="number" placeholder="Enter email" />
                                        </Form.Group>
                                        <Form.Group >
                                            <Form.Label>Email address</Form.Label>
                                            <Form.Control type="email" placeholder="Enter email" />
                                        </Form.Group>
                                        <Form.Group controlId="formGridState">
                                            <Form.Label>Role</Form.Label>
                                            <Form.Control as="select" defaultValue="Choose...">
                                                <option>Choose...</option>
                                                <option>Admin</option>
                                                <option>Doctors</option>
                                                <option>Receptionist/Job Admin</option>
                                                <option>Collectors</option>
                                                <option>Lab Assistants</option>
                                                <option>Accounts</option>

                                            </Form.Control>
                                        </Form.Group>

                                        {/* <Form.Group controlId="formBasicCheckbox">
                                            <Form.Check type="checkbox" label="Show Password" onChange={this.showPassword} />
                                        </Form.Group> */}
                                        <Row className="justify-content-around">

                                            <Button variant="primary" onClick={this.gotoRegister}>  Submit </Button>
                                        </Row>

                                    </Form>
                                </Card.Body>
                            </Card>
                        </Row>
                    </Container>
                </ThemeProvider>
            </div>
        )
    }
}

export default UserRegister
